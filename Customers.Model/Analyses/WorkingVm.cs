﻿using Customers.Resources.Analyses;
using Customers.Resources.BasicInput;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Analyses
{
   public class WorkingVm
    {
        [Display(ResourceType = typeof(AnalysesRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "StartDate")]

        public DateTime? StartDate { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "EndDate")]

        public DateTime? EndDate { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "StartHour")]

        public string StartHour { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "EndHour")]

        public int RequestOffersId { get; set; }
        public string EndHour { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }


        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }

    }
}
