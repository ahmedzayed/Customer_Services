﻿using Customers.Resources.Analyses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Analyses
{
   public class AnalysesRequestDetailsVm
    {
        [Display(ResourceType = typeof(AnalysesRes), Name = "Id")]
        public int Id { get; set; }

        public int RequestId { get; set; }

        [Display(ResourceType = typeof(AnalysesRes), Name = "FileName")]

        public string FileName { get; set; }

        public string CustName { get; set; }

        public string AnalysesName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
