﻿using Customers.Resources.Analyses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Analyses
{
   public class AnalysesVisitsVm
    {
        [Display(ResourceType = typeof(AnalysesRes), Name = "Id")]
        public int Id { get; set; }
       


        public int AnalysesRequestId { get; set; }


        [Display(ResourceType = typeof(AnalysesRes), Name = "Nots")]

        public string    Nots { get; set; }

        [Display(ResourceType = typeof(AnalysesRes), Name = "Image")]

        public string Image { get; set; }


       

        [Display(ResourceType = typeof(AnalysesRes), Name = "Visitdate")]

        public DateTime? Visitdate { get; set; }


       


         public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }


    }
}
