﻿using Customers.Resources.Analyses;
using Customers.Resources.BasicInput;
using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Analyses
{
  public  class AnalysesRequestVm
    {

        [Display(ResourceType = typeof(AnalysesRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(AnalysesRes), Name = "CustId")]

        public int CustomerId { get; set; }
        [Display(ResourceType = typeof(AnalysesRes), Name = "CustId")]

        public string CustName { get; set; }
        [Display(ResourceType = typeof(AnalysesRes), Name = "CustomerAnalysesId")]
        [UIHint("DropDownList")]

        public int CustomerAnalysesId { get; set; }


        [Display(ResourceType = typeof(BasicInputRes), Name = "EngId")]
        [UIHint("DropDownList")]

        public int? EngId { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "EngId")]

        public string EngName { get; set; }
        [Display(ResourceType = typeof(AnalysesRes), Name = "CustomerAnalysesId")]

        public string CustomerAnalysesName { get; set; }
        [Display(ResourceType = typeof(AnalysesRes), Name = "AnalysesDate")]

        public DateTime? AnalysesDate { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
