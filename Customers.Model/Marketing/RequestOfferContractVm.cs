﻿using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
  public  class RequestOfferContractVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }



        public int RequestOfferId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ContractId")]
        [UIHint("DropDownList")]

        public int? ContractId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "ContractId")]

        public string ContractName { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "StartDate")]

        public DateTime? StartDate { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "EndDate")]

        public DateTime? EndDate { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ServiceName")]

        public string ServiceName { get; set; }


        [Display(ResourceType = typeof(MarketingRes), Name = "CustId")]

        public string CustName { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "CustId")]

        public int CustomerId { get; set; }

        public int RequestId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ActivityId")]

        public string ActivityName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "EmployeeId")]

        public string EmployeeName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "Note")]

        public string RequestStatusName { get; set; }






        public DateTime? CreatedDate { get; set; }


        public int? CreatedBy { get; set; }
    }
}
