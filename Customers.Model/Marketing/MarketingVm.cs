﻿using Customers.Resources.BasicInput;
using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
   public class MarketingVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "CustId")]
        [UIHint("DropDownList")]

        public int CustId { get; set; }
      [Display(ResourceType = typeof(MarketingRes), Name = "CustId")]

        public string CustName { get; set; }


        [Display(ResourceType = typeof(MarketingRes), Name = "ActivityId")]
        [UIHint("DropDownList")]

        public int ActivityId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ActivityId")]

        public string ActivityName { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "EmployeeId")]
        [UIHint("DropDownList")]

        public int? EmployeeId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "EmployeeId")]

        public string EmployeeName{ get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "RequestStatusId")]
        [UIHint("DropDownList")]

        public int RequestStatusId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "Note")]

        public string RequestStatusName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "Note")]

        public string Note { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
    }
}
