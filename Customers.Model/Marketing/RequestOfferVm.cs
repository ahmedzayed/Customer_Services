﻿using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
  public  class RequestOfferVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "RequestId")]


        public int RequestId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "OfferId")]
        [UIHint("DropDownList")]

        public int? OfferId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "OfferName")]

        public string OfferName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ServiceName")]

        public string ServiceName { get; set; }


        [Display(ResourceType = typeof(MarketingRes), Name = "CustId")]

        public string CustName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ActivityId")]

        public string ActivityName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "EmployeeId")]

        public string EmployeeName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "Note")]

        public string RequestStatusName { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "OfferSelected")]

        public bool? OfferSelected { get; set; }

        public DateTime? CreatedDate { get; set; }


        public int? CreatedBy { get; set; }
    }
}
