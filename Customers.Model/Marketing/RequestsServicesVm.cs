﻿using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
 public   class RequestsServicesVm
    {

        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "RequestId")]
       

        public int RequestId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ServiceId")]
         [UIHint("DropDownList")]

        public int ServiceId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "ServiceId")]

        public string ServiceName { get; set; }

        public DateTime? CreatedDate { get; set; }

    
        public int? CreatedBy { get; set; }
    }
}
