﻿using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
   public class RequestOfferConditionVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }



        public int RequestOfferId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "ConditionId")]
        [UIHint("DropDownList")]

        public int? ConditionId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "ConditionId")]

        public string ConditionName { get; set; }


      

        public DateTime? CreatedDate { get; set; }


        public int? CreatedBy { get; set; }
    }
}
