﻿using Customers.Resources.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Marketing
{
   public class RequestStatusVm
    {

        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "RequestId")]


        public int RequestId { get; set; }

        [Display(ResourceType = typeof(MarketingRes), Name = "StatusId")]
        [UIHint("DropDownList")]

        public int StatusId { get; set; }
        [Display(ResourceType = typeof(MarketingRes), Name = "StatusId")]

        public string StatusName { get; set; }


        public DateTime? CreatedDate { get; set; }


        public int? CreatedBy { get; set; }



    }
}
