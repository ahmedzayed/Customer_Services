﻿using Customers.Resources.EngineeringTask;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.EngineeringTask
{
  public  class EngineeringTaskVm
    {
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderNumber")]

        public string OrderNumber { get; set; }


        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderAddress")]

        public string OrderAddress { get; set; }

        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderDetails")]

        public string OrderDetails { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderDate")]

        public DateTime? OrderDate { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderChangeDate")]

        public DateTime? OrderChangeDate { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderCase")]

        public int? OrderCase { get; set; }

        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "OrderFile")]

        public string OrderFile { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "EmployeeId")]
        [UIHint("DropDownList")]
        public int EmployeeId { get; set; }
        [Display(ResourceType = typeof(EngineeringTaskRes), Name = "EndDate")]

        public DateTime? EndDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
}
