﻿using Customers.Resources.BasicInput;
using Customers.Resources.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.BasicInput
{
   public class ContractsItemsVm
    {
        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(ContractsRe), Name = "ContractsName")]

         public int? ContractsId { get; set; }
        [Display(ResourceType = typeof(ContractsRe), Name = "ItemsName")]
        [UIHint("DropDownList")]

        public int? ItemsId { get; set; }

        public string ItemsName { get; set; }

        public int CreatedBy { get; set; }
    }
}
