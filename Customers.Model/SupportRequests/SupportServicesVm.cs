﻿using Customers.Resources.Marketing;
using Customers.Resources.SupportRequests;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.SupportRequests
{
  public  class SupportServicesVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "ServicesId")]
        [UIHint("DropDownList")]
        public int ServicesId { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "ServicesId")]

        public string    ServicesName{ get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "Description")]

        public string Description { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "DepartmentId")]
        [UIHint("DropDownList")]

        public int DepartmentId { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "DepartmentId")]

        public string DepartmentName { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "Image")]

        public string Image { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "CustomerId")]

        public int CustomerId { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "HourDate")]

        public string HourDate { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "Date")]

        public DateTime? CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "EmployeeId")]
        [UIHint("DropDownList")]
        public int? EmployeeId { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "EmployeeId")]

        public string EmployeeName { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "DetailsService")]

        public string Details { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "CustomerId")]

        public string CustomerName { get; set; }
    }
}
