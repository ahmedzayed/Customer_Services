﻿using Customers.Resources.Marketing;
using Customers.Resources.SupportRequests;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.SupportRequests
{
  public  class SupportVisitVm
    {
        [Display(ResourceType = typeof(MarketingRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "VisitReason")]
        public string VisitReason { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "SuggestedTime")]
        public DateTime? SuggestedTime { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "VisitDate")]
        public DateTime? VisitDate { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "VisitHoure")]
        public string VisitHoure { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "CustomerId")]
        public int CustomerId { get; set; }

        public int CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }


        [Display(ResourceType = typeof(SupportRequestsRes), Name = "EmployeeId")]
        [UIHint("DropDownList")]
        public int? EmployeeId { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "EmployeeId")]

        public string EmployeeName { get; set; }
        [Display(ResourceType = typeof(SupportRequestsRes), Name = "DetailsVisit")]

        public string Details { get; set; }

        [Display(ResourceType = typeof(SupportRequestsRes), Name = "CustomerId")]

        public string CustomerName { get; set; }
    }

}
