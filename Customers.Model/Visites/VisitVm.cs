﻿using Customers.Resources.Visites;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Visites
{
  public  class VisitVm
    {
        [Display(ResourceType = typeof(VisitRes), Name = "Id")]
        public int Id { get; set; }


        [Display(ResourceType = typeof(VisitRes), Name = "CustomerId")]
        public int CustomerId { get; set; }
        [Display(ResourceType = typeof(VisitRes), Name = "CustomerId")]

        public string CustomerName { get; set; }


        [Display(ResourceType = typeof(VisitRes), Name = "EmployeeId")]
        public int EmployeeId { get; set; }
        [Display(ResourceType = typeof(VisitRes), Name = "EmployeeId")]

        public string    EmployeeName { get; set; }

        [Display(ResourceType = typeof(VisitRes), Name = "VisitDetails")]
        public string VisitDetails { get; set; }

        [Display(ResourceType = typeof(VisitRes), Name = "ImportantInformation")]
        public string ImportantInformation { get; set; }

        [Display(ResourceType = typeof(VisitRes), Name = "IsFix")]
        public bool? IsFix { get; set; }

        public DateTime? VisitDate { get; set; }

        public DateTime?     CreatedDate { get; set; }

        public int CreatedBy { get; set; }
    }
}
