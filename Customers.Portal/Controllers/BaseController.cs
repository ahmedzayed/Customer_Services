﻿using Customers.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Controllers
{
    public class BaseController : Controller
    {
        //protected virtual new CustomPrincipal User
        //{
        //    get { return HttpContext.User as CustomPrincipal; }
        //}
        public ActionResult Test()
        {
            if(Customers.Helpers.Utilities.ResourcesReader.UserId == null)
            {
                RedirectToAction("Login", "Account");
            }
            else
            {
                return null;
            }
            return null;
        }
        public ActionResult ToggleLanguage(string returnUrl)
        {
            Sessions.CurrentCulture = Sessions.CurrentCulture == "ar-EG" ? "en-CA" : "ar-EG";
            if (Sessions.CurrentCulture == "en-CA")
            {
                Sessions.CurrentText = "العربية";
            }
            else if (Sessions.CurrentCulture == "ar-EG")
            {
                Sessions.CurrentText = "English";

            }

            return Redirect(returnUrl);
        }
	}
}