﻿using Customers.Abstracts.Marketing;
using Customers.Abstracts.SupportRequests;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Controllers
{
    public class HomeController : BaseController
    {
        // GET: BasicInput/Shared

        private readonly IRequestService _RequestService;
        private readonly IRequestOfferService _ReqOfferService;
        private readonly IRequestContractService _RequestContracService;
        private readonly ISupportRequestService _SupportRequestService;
        private readonly ISupportVisitService _SupportVisitService;
        private readonly ISupportServicesService _supportService;
        public HomeController(IRequestService RequestService,IRequestContractService RequestContract,IRequestOfferService RequestOfferServie,ISupportRequestService SupportRequestService, ISupportVisitService SupportVisit,ISupportServicesService SupportService)
        {
            _RequestService = RequestService;
            _RequestContracService = RequestContract;
            _ReqOfferService = RequestOfferServie;
            _SupportRequestService = SupportRequestService;
            _supportService = SupportService;
            _SupportVisitService = SupportVisit;
        }
        public ActionResult _SideMenu()
        {
            ViewBag.Marketing = _RequestService.Count();
            ViewBag.OfferPrice = _ReqOfferService.Count();
            ViewBag.Contract = _RequestContracService.Count();
            ViewBag.SupportVist = _SupportVisitService.Count();
            ViewBag.SupportRequest = _SupportRequestService.Count();
            ViewBag.SupportService = _supportService.Count();
            ViewBag.AllSuport = ViewBag.SupportVist + ViewBag.SupportRequest + ViewBag.SupportService;


            return PartialView();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
      
      
    }
}