﻿using Customers.Abstracts;
using Customers.Model;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace Customers.Portal.Utilities
{
    public class CommonService 
    {

        private readonly ILookupService _lookupService;
        public CommonService(ILookupService lookupService)
        {

            _lookupService = lookupService;
        }
        private static SelectListItem Clone(KeyValueLookup model, int? selectedValue)
        {
            return new SelectListItem
            {
                Text = model.Text,
                Value = model.Value.ToString(),
                Selected = selectedValue == model.Value
            };
        }

        #region BasicInput
        internal List<SelectListItem> FindCustomerActivities(int selectedValue = 0)
        {
            var categories = _lookupService.FindCustomerActivities();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindServices(int selectedValue = 0)
        {
            var categories = _lookupService.FindServices();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindWorkSystem(int selectedValue = 0)
        {
            var categories = _lookupService.FindWorkSystem();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindEmployeeType(int selectedValue = 0)
        {
            var categories = _lookupService.FindEmployeeType();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindSpecialization(int selectedValue = 0)
        {
            var categories = _lookupService.FindSpecialization();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }

        internal List<SelectListItem> FindItems(int selectedValue = 0)
        {
            var categories = _lookupService.FindItems();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindConditions(int selectedValue = 0)
        {
            var categories = _lookupService.FindConditions();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindDepartments(int selectedValue = 0)
        {
            var categories = _lookupService.FindDepartments();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
       
        internal List<SelectListItem> FindCustomeranalyses(int selectedValue = 0)
        {
            var categories = _lookupService.FindCustomerAnalyses();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindContract(int selectedValue = 0)
        {
            var categories = _lookupService.FindContract();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }


        internal List<SelectListItem> FindConditionsEmp(int selectedValue = 0)
        {
            var categories = _lookupService.FindConditionsEmp();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }

        #endregion
        #region Marketing
        internal List<SelectListItem> FindEmployees(int selectedValue = 0)
        {
            var categories = _lookupService.FindEmployees();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindEngineeringEmp(int selectedValue = 0)
        {
            var categories = _lookupService.FindEngineeringEmp();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindCustomers(int selectedValue = 0)
        {
            var categories = _lookupService.FindCustomers();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        internal List<SelectListItem> FindCustomerStatus(int selectedValue = 0)
        {
            var categories = _lookupService.FindCustomerStatus();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }

        internal List<SelectListItem> FindOffer(int selectedValue = 0)
        {
            var categories = _lookupService.FindOffer();
            var list = new List<SelectListItem>();
            list.AddRange(categories.Select(m => Clone(m, selectedValue)).ToList());
            return list;
        }
        #endregion


    }
}