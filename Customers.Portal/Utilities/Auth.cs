﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Customers.Portal.Utilities
{
    public class Auth : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            if (Roles.Any(role.Contains))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Auth(string username)
        {
            this.Identity = new GenericIdentity(username);
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }
        public bool IsSysAdmin { get; set; }
    }

    public class CustomPrincipalSerializeModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }
        public bool IsSysAdmin { get; set; }
    }
}