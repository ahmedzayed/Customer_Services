﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Customers.Portal.Utilities
{
    public class CustomAuthorizeAttribute: AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (Customers.Helpers.Utilities.ResourcesReader.UserId == 0)
            {
                filterContext.Result = new RedirectToRouteResult(
                                           new RouteValueDictionary
                                           { { "area",string.Empty},
                                           { "action", "Login" },
                                           { "controller", "Account" },
                                           });
            }
        }
    }
}