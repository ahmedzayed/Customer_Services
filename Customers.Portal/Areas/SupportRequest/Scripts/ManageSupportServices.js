﻿var ManageSupportServices = {
    init: function () {
        Search();
    },
}

function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    $form.find('input[type="file"]').each(function (count, fileInput) {
        var files = $(fileInput).get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            $.each(files, function (index, file) {
                data.append($(fileInput).attr("name") + "[" + index + "]", file);
            });
        }
    });
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    //if (IsValid()) {
    ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

        var res = result.split(',');
        //debugger;
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            //window.location = "../ManageAnalysesRequest/Index1";
            toastr.success(res[1]);
            Search();
        }
        else
            toastr.error(res[1]);
    });
    //  }
    //}
}
//function Save() {

//    var $form = $("#ModelForm");
//    var data = new FormData();
//    var formData = $form.serializeArray();
//    $.each(formData, function (key, value) {
//        data.append(this.name, this.value);
//    });

//    if (IsValid()) {
//        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

//            var res = result.split(',');
//            //debugger;
//            if (res[0] == "success") {
//                $('#myModalAddEdit').modal('hide');
//                //window.location = "../ManageItems/Index";
//                toastr.success(res[1]);
//                Search();
//            }
//            else
//                toastr.error(res[1]);
//        });
//        //  }
//    }
//}
function Create() {
    var Url = "../ManageSupportServices/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../ManageSupportServices/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function AddDetails(id) {
    var Url = "../ManageSupportServices/AddDetails?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });

}
function ChangeToEmployee(id) {
    var Url = "../ManageSupportServices/ChangeToEmployee?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });

}
function Delete(id) {
    var Url = "../ManageSupportServices/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../ManageSupportServices/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageItems/Index";
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageSupportServices/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


function GetContact(id) {
    var Url = "../ManageRequestOffer/GetContact?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

