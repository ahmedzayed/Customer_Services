﻿using Customers.Abstracts;
using Customers.Abstracts.SupportRequests;
using Customers.Model.SupportRequests;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.SupportRequest.Controllers
{
    [CustomAuthorize]

    public class ManageSupportVisitController : BaseController
    {
        #region Services

        private readonly ISupportVisitService _thisService;
        private readonly CommonService _commonService;


        #endregion

        #region Constructor
        public ManageSupportVisitController(ISupportVisitService thisService, ILookupService lookupService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);

        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                ViewBag.userlogin = 1;

            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                ViewBag.userlogin = 2;
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                ViewBag.userlogin = 1;
            }
            else
                ViewBag.userlogin = 1;
            return View();
        }
        public ActionResult Search(SupportVisitSm searchModel ,int?CustomerId)
        {

            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                var result = _thisService.FindAll(searchModel);
                return PartialView(result);
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                CustomerId = Customers.Helpers.Utilities.ResourcesReader.UserId;
                var result = _thisService.GetByCustomerId((int)CustomerId);
                return PartialView(result);
            }

            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                var result = _thisService.GetByEmployeeId((int)CustomerId);
                return PartialView(result);
            }
            else
            return PartialView();
        }

        public ActionResult Create()
        {
            SupportVisitVm Obj = new SupportVisitVm();
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportVisit/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(SupportVisitVm viewModel)
        {

           

            viewModel.CustomerId = 4;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            SupportVisitVm obj = _thisService.GetById(id);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportVisit/Create.cshtml", obj);
        }
        public ActionResult AddVisitDetails(int Id)
        {
            SupportVisitVm obj = _thisService.GetById(Id);

            return PartialView("~/Areas/SupportRequest/Views/ManageSupportVisit/AddVisitDetails.cshtml", obj);


        }
        [HttpPost]
        public JsonResult AddVisitDetails(SupportVisitVm viewModel)
        {


            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult ChangeToEmployee(int Id)
        {

            SupportVisitVm obj = _thisService.GetById(Id);
            ViewBag.EmployeeId = _commonService.FindEmployees((int)obj.EmployeeId);


            return PartialView("~/Areas/SupportRequest/Views/ManageSupportVisit/ChangeToEmployee.cshtml", obj);
        }
        [HttpPost]
        public JsonResult ChangeToEmployee(SupportVisitVm viewModel)
        {

            //viewModel.EmployeeId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            SupportRequestsVm obj = new SupportRequestsVm();
            obj.Id = id;
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportVisit/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}