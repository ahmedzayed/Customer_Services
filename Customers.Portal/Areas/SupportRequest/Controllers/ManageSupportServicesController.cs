﻿using Customers.Abstracts;
using Customers.Abstracts.SupportRequests;
using Customers.Model.SupportRequests;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.SupportRequest.Controllers
{
    [CustomAuthorize]

    public class ManageSupportServicesController : BaseController
    {
        #region Services

        private readonly ISupportServicesService _thisService;
        private readonly CommonService _commonService;


        #endregion

        #region Constructor
        public ManageSupportServicesController(ISupportServicesService thisService, ILookupService lookupService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);

        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                ViewBag.userlogin = 1;

            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                ViewBag.userlogin = 2;
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                ViewBag.userlogin = 1;
            }
            else
                ViewBag.userlogin = 1;
            return View();
        }
        public ActionResult Search(SupportServicesSm searchModel, int? CustomerId)
        {

            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                var result = _thisService.FindAll(searchModel);
                return PartialView(result);
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                CustomerId = Customers.Helpers.Utilities.ResourcesReader.UserId;
                var result = _thisService.GetByCustomerId((int)CustomerId);
                return PartialView(result);
            }

            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                var result = _thisService.GetByEmployeeId((int)CustomerId);
                return PartialView(result);
            }
            else
                return PartialView();
        }


        public ActionResult Create()
        {
            SupportServicesVm Obj = new SupportServicesVm();
            ViewBag.DepartmentId = _commonService.FindDepartments();
            ViewBag.ServicesId = _commonService.FindServices();

            return PartialView("~/Areas/SupportRequest/Views/ManageSupportServices/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(SupportServicesVm viewModel)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploaded/SupportServices/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    viewModel.Image = fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

            viewModel.CustomerId = 4;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            SupportServicesVm obj = _thisService.GetById(id);
            ViewBag.DepartmentId = _commonService.FindDepartments(obj.DepartmentId);
            ViewBag.ServicesId = _commonService.FindServices(obj.ServicesId);


            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportServices/Create.cshtml", obj);
        }
        public ActionResult AddDetails(int Id)
        {
            SupportServicesVm obj = _thisService.GetById(Id);

            return PartialView("~/Areas/SupportRequest/Views/ManageSupportServices/AddDetails.cshtml", obj);


        }
        [HttpPost]
        public JsonResult AddDetails(SupportServicesVm viewModel)
        {


            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult ChangeToEmployee(int Id)
        {

            SupportServicesVm obj = _thisService.GetById(Id);
            ViewBag.EmployeeId = _commonService.FindEmployees((int)obj.EmployeeId);


            return PartialView("~/Areas/SupportRequest/Views/ManageSupportServices/ChangeToEmployee.cshtml", obj);
        }
        [HttpPost]
        public JsonResult ChangeToEmployee(SupportServicesVm viewModel)
        {

            //viewModel.EmployeeId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }



        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            SupportServicesVm obj = new SupportServicesVm();
            obj.Id = id;
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportServices/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}