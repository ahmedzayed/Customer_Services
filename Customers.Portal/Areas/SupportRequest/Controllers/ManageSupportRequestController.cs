﻿using Customers.Abstracts;
using Customers.Abstracts.SupportRequests;
using Customers.Model.SupportRequests;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.SupportRequest.Controllers
{
    [CustomAuthorize]

    public class ManageSupportRequestController : BaseController
    {
        #region Services

        private readonly ISupportRequestService _thisService;
        private readonly CommonService _commonService;


        #endregion

        #region Constructor
        public ManageSupportRequestController(ISupportRequestService thisService, ILookupService lookupService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);

        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                ViewBag.userlogin = 1;

            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                ViewBag.userlogin = 2;
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                ViewBag.userlogin = 1;
            }
            else
                ViewBag.userlogin = 1;
            return View();
        }
        public ActionResult Search(int? customerId,SupportRequestsSm viewmodel)
        {

            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                ViewBag.userlogin = 1;
                var result = _thisService.FindAll(viewmodel);
                return PartialView(result);
            }
            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 2)
            {
                ViewBag.userlogin = 2;
                customerId = Customers.Helpers.Utilities.ResourcesReader.UserId;
                var result = _thisService.GetByCustomerId((int)customerId);
                return PartialView(result);
            }

            else if (Customers.Helpers.Utilities.ResourcesReader.UserTypeId == 1)
            {
                ViewBag.userlogin = 1;
                var result = _thisService.GetByEmployeeId((int)customerId);
                return PartialView(result);
            }
            else
                ViewBag.userlogin = 1;
                return PartialView();

        }

        public ActionResult Create()
        {
            SupportRequestsVm Obj = new SupportRequestsVm();
            ViewBag.DepartmentId = _commonService.FindDepartments();
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportRequest/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(SupportRequestsVm viewModel)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploaded/SupportRequests/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    viewModel.Image = fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

            viewModel.CustomerId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            SupportRequestsVm obj = _thisService.GetById(id);
            ViewBag.DepartmentId = _commonService.FindDepartments(obj.DepartmentId);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportRequest/Create.cshtml", obj);
        }

        public ActionResult AddDetails(int Id)
        {
            SupportRequestsVm obj = _thisService.GetById(Id);
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportRequest/AddDetails.cshtml", obj);

        }
        [HttpPost]
        public JsonResult AddDetails(SupportRequestsVm viewModel)
        {

            
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult ChangeToEmployee(int Id)
        {

            SupportRequestsVm obj = _thisService.GetById(Id);
            ViewBag.EmployeeId = _commonService.FindEmployees((int)obj.EmployeeId);


            return PartialView("~/Areas/SupportRequest/Views/ManageSupportRequest/ChangeToEmployee.cshtml", obj);
        }
        [HttpPost]
        public JsonResult ChangeToEmployee(SupportRequestsVm viewModel)
        {

            //viewModel.EmployeeId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            SupportRequestsVm obj = new SupportRequestsVm();
            obj.Id = id;
            return PartialView("~/Areas/SupportRequest/Views/ManageSupportRequest/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}