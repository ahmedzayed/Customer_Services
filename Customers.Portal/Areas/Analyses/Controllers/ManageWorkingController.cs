﻿using Customers.Abstracts;
using Customers.Abstracts.Analyses;
using Customers.Abstracts.BasicInput;
using Customers.Abstracts.Marketing;
using Customers.Model.Analyses;
using Customers.Model.Marketing;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Analyses.Controllers
{
    [CustomAuthorize]

    public class ManageWorkingController : BaseController
    {
        #region Services

        private readonly IRequestContractService _RequestContractService;
        private readonly IRequestOfferContractService _RequestOfferContractService;
        private readonly CommonService _commonService;
        private readonly IRequestService _RequestService;
        private readonly IWorkingService _ThisService;

        private readonly ICommunicationOfficerService _CommService;
        private readonly IRequestServicesService _RequestServicesService;

        #endregion

        #region Constructor
        public ManageWorkingController(IRequestContractService RequestContractService, IRequestService RequestService,IWorkingService thisService,
            IRequestServicesService RequestServicesService, ICommunicationOfficerService CommService, ILookupService lookupService, IRequestOfferContractService RequestOfferContractService)
        {
            _RequestContractService = RequestContractService;
            _RequestService = RequestService;
            _CommService = CommService;
            _RequestServicesService = RequestServicesService;
            _ThisService = thisService;

            _RequestOfferContractService = RequestOfferContractService;
            _commonService = new CommonService(lookupService);
        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(RequestOfferContractSm searchModel)
        {
            var result = _RequestContractService.FindAll(searchModel);
            return PartialView(result);
        }

        #endregion
        public ActionResult GetContact(int Id)
        {
            int Custid = _RequestService.GetById(Id).CustId;
            var Obj = _CommService.GetByCustomerId(Id);

            return PartialView("GetContact", Obj);
        }

        public ActionResult AddWorking(int RequestOfferId, int CustomerId, int ContractId)
        {
            WorkingVm obj = new WorkingVm();
            obj.CustomerId = CustomerId;
            obj.ContractId = ContractId;
            obj.RequestOffersId = RequestOfferId;
            return View(obj);
        }
        [HttpPost]
        public JsonResult AddWorking(WorkingVm viewModel)
        {

            if (_ThisService.Save(viewModel))
            {
                return Json("AddWorking," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


        }
        //public ActionResult Create( int RequestOfferId,int CustomerId,int ContractId)
        //{
        //    WorkingVm obj = new WorkingVm();
        //    obj.CustomerId = CustomerId;
        //    obj.ContractId = ContractId;
        //    obj.RequestOffersId = RequestOfferId;
        //    return PartialView("~/Areas/Analyses/Views/ManageWorking/Create.cshtml", obj);
        //}

      
        public ActionResult GetWorkingList(int CustomerId,int RequestId)
        {
            var obj = _ThisService.GetWorkingList(CustomerId,RequestId);
            return PartialView("GetWorkingList", obj);
        }
      
        //public JsonResult Create(WorkingVm viewModel)
        //{
            
        //    if (_ThisService.Save(viewModel))
        //    {
        //        return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


        //}
        public ActionResult GetServices(int Id)
        {

            var Obj = _RequestServicesService.GetServicesByRequestId(Id);

            return PartialView("GetServices", Obj);
        }

        [HttpGet]
        public ActionResult DeleteWorking(int id)
        {
            WorkingVm obj = new WorkingVm();
            obj.Id = id;
            return PartialView("~/Areas/Analyses/Views/ManageWorking/DeleteWorking.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteWorkingRow(int Id)
        {
            if (_ThisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
    }
}