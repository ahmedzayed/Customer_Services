﻿using Customers.Abstracts;
using Customers.Abstracts.Analyses;
using Customers.Abstracts.BasicInput;
using Customers.Abstracts.Marketing;
using Customers.Model.Analyses;
using Customers.Model.BasicInput;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Analyses.Controllers
{
    [CustomAuthorize]
    public class ManageAnalysesRequestController : BaseController
    {
        #region Services

        private readonly ICustomerService _CustomerService;
        private readonly IAnalysesRequestService _thisService;
        private readonly CommonService _commonService;
        private readonly IRequestServicesService _requestService;
        private readonly IRequestContractService _requestContractService;
        private readonly IAnalysesRequestDetailsService _AnalysesRequestService;
        private readonly IAnalysesVisitService _VisitService;

        private readonly ICommunicationOfficerService _communicateservice;

        #endregion

        #region Constructor
        public ManageAnalysesRequestController(ICustomerService CustomerService,IAnalysesVisitService VisitService,
            IRequestContractService requestContractService, IRequestServicesService requestservice,IAnalysesRequestDetailsService AnalysesRequestService,
            ICommunicationOfficerService commservice, ILookupService lookupService, IAnalysesRequestService thisService)
        {
            _thisService = thisService;
            _AnalysesRequestService = AnalysesRequestService;
            _requestService = requestservice;
            _VisitService = VisitService;
            _communicateservice = commservice;
            _requestContractService = requestContractService;
            _CustomerService = CustomerService;
            _commonService = new CommonService(lookupService);
        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(CustomerSm searchModel)
        {
            var result = _CustomerService.FindAll(searchModel);
            return PartialView(result);
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult Search2(AnalysesRequestSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            AnalysesRequestVm obj = new AnalysesRequestVm();
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/Create.cshtml", obj);
        }
        [HttpPost]
        public JsonResult Create(AnalysesRequestVm viewModel)
        {

           
           
            if (_thisService.Save(viewModel))
                {
                CustomerVm C = _CustomerService.GetById(viewModel.CustomerId);
                C.IsAnalyses = true;
                _CustomerService.Save(C);
               return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            

        }


        public ActionResult CreateAttachment()
        {
            AnalysesRequestVm obj = new AnalysesRequestVm();
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/CreateAttachment.cshtml", obj);
        }
        [HttpPost]
        public JsonResult CreateAttachment(AnalysesRequestVm viewModel)
        {


          
            if (_thisService.Save(viewModel))
            {
                CustomerVm C = _CustomerService.GetById(viewModel.CustomerId);
                C.IsAnalyses = true;
                _CustomerService.Save(C);
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


        }

        public ActionResult Edit(int id = 0)
        {
            AnalysesRequestVm obj = new AnalysesRequestVm();
            obj.CustomerId = id;

            ViewBag.CustomerAnalysesId = _commonService.FindCustomeranalyses();

            //ViewBag.CustomerAnalysesId = _commonService.FindContract((int)obj.CustomerAnalysesId);
            //if (obj == null)
            //{
            //    return HttpNotFound();
            //}
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/Create.cshtml", obj);
        }

        public ActionResult GetAnalysesList(int CustomerId)
        {
            AnalysesRequestSm searchModel = new AnalysesRequestSm();


            var Data = _thisService.GetByCustomerId(CustomerId);

            return PartialView("_GetAnalysesList", Data);
        }


        public ActionResult GetAnalysesData()
        {
            ViewBag.CustomerAnalysesId = _commonService.FindCustomeranalyses();
            return PartialView("AnalysesData");
        }

        [HttpGet]
        public ActionResult DeleteAnalyses(int id)
        {
            AnalysesRequestVm obj = new AnalysesRequestVm();
            obj.Id = id;
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/DeleteAnalyses.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteAnalysesRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region ActionHelper
        public ActionResult GetAnalysesFileList(int RequestId)
        {
            var obj = _AnalysesRequestService.GetByRequestId(RequestId);
            return PartialView("GetAnalysesFileList", obj);
        }

        public ActionResult CreateAnalysesDetails(int Id)
        {
            //var obj = _AnalysesRequestService.GetByRequestId(Id);
            AnalysesRequestDetailsVm AD = new AnalysesRequestDetailsVm();
            AD.RequestId = Id;
            
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/CreateAnalysesDetails.cshtml", AD);

        }
        [HttpPost]
        public JsonResult CreateAnalysesDetails(AnalysesRequestDetailsVm viewmodel)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploaded/AnalysesFile/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    viewmodel.FileName = fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
            viewmodel.CreatedDate = DateTime.Now;
            viewmodel.UpdatedDate = DateTime.Now;
            if (_AnalysesRequestService.Save(viewmodel))
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetContact(int Id)
        {
            var Obj = _communicateservice.GetByCustomerId(Id);

            return PartialView("GetContact", Obj);
        }

        public ActionResult GetServices(int Id)
        {

            var Obj = _requestService.GetServicesByCustomerId(Id);

            return PartialView("GetServices", Obj);
        }

        public ActionResult GetContract(int Id)
        {

            var Obj = _requestContractService.GetByCustomerId(Id);

            return PartialView("GetContract", Obj);
        }


        #endregion

        #region Visited
        public ActionResult GetVisitList(int RequestId)
        {
            var obj = _VisitService.GetByRequestId(RequestId);
            return PartialView("GetVisitList", obj);
        }
        public ActionResult CreateVisit(int Id)
        {
            //var obj = _AnalysesRequestService.GetByRequestId(Id);
            AnalysesVisitsVm AV = new AnalysesVisitsVm();
            AV.AnalysesRequestId = Id;

            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/CreateVisit.cshtml", AV);

        }


        [HttpPost]
        public JsonResult CreateVisit(AnalysesVisitsVm viewmodel)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploaded/AnalysesFile/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    viewmodel.Image = fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
            viewmodel.CreatedDate = DateTime.Now;
            viewmodel.UpdatedDate = DateTime.Now;
            if (_VisitService.Save(viewmodel))
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteVisit(int id)
        {
            AnalysesVisitsVm obj = new AnalysesVisitsVm();
            obj.Id = id;
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/DeleteVisit.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteVisitRow(int Id)
        {
            if (_VisitService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteFile(int id)
        {
            AnalysesRequestDetailsVm obj = new AnalysesRequestDetailsVm();
            obj.Id = id;
            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/DeleteFile.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteFileRow(int Id)
        {
            if (_AnalysesRequestService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEng(int Id)
        {
            //var obj = _AnalysesRequestService.GetByRequestId(Id);
            AnalysesRequestVm AV = _thisService.GetById(Id);
            ViewBag.EngId = _commonService.FindEngineeringEmp();

            return PartialView("~/Areas/Analyses/Views/ManageAnalysesRequest/AddEng.cshtml", AV);

        }
        [HttpPost]
        public JsonResult AddEng(AnalysesRequestVm viewModel)
        {
            
            if (_thisService.Save(viewModel))
            {
               
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


        }

        #endregion
    }
}
