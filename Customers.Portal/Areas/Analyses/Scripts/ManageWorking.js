﻿


var ManageWorking = {
    init: function () {
        Search();
    },
}
function Save() {

    var $form = $("#ModelForm");
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                window.location = "../ManageWorking/Index1";
                toastr.success(res[1]);
                Search();
            }
            else if (res[0] == "AddWorking") {
                toastr.success("تك اضافه الخطه اليوميه بنجاح");
                location.reload();
            }
            else
                toastr.error(res[1]);
        });
        //  }
    }
}

//function Edit(RequestOfferId,CustomerId, ContractId) {
//    var Url = "../../../ManageWorking/Create?RequestOfferId=" + RequestOfferId + "&CustomerId=" + CustomerId + "&ContractId=" + ContractId;
//    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
//        //$('#myModalAddEdit').modal('show');
//    });
//}




//function Edit(id) {
//    var Url = "../ManageWorking/Edit?Id=" + id;
//    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
//        $('#myModalAddEdit').modal('show');
//    });
//}


function DeleteWorking(id) {
    var Url = "../../../ManageWorking/DeleteWorking?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteWorkingRow(id) {
    var Url = "../../../ManageWorking/DeleteWorkingRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            location.reload();
        }
        else
            toastr.error(res[1]);
    });
}



function GetContact(id) {
    var Url = "../ManageWorking/GetContact?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function GetServices(id) {
    var Url = "../ManageWorking/GetServices?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageWorking/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}
