﻿var ManageAnalysesRequest = {
    init: function () {
        Search();
    },
}
function Save() {

    var $form = $("#ModelForm");
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                window.location = "../ManageAnalysesRequest/Index1";
                toastr.success(res[1]);
                Search();
            }
            else
                toastr.error(res[1]);
        });
        //  }
    }
}


function SaveWitheFile() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    $form.find('input[type="file"]').each(function (count, fileInput) {
        var files = $(fileInput).get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            $.each(files, function (index, file) {
                data.append($(fileInput).attr("name") + "[" + index + "]", file);
            });
        }
    });
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    //if (IsValid()) {
    ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

        var res = result.split(',');
        //debugger;
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            window.location = "../ManageAnalysesRequest/Index1";
            toastr.success(res[1]);
            //Search();
        }
        else
            toastr.error(res[1]);
    });
    //  }
    //}
}
function Create() {
    var Url = "../ManageAnalysesRequest/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}




function Edit(id) {
    var Url = "../ManageAnalysesRequest/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function CreateVisit(id) {
    var Url = "../ManageAnalysesRequest/CreateVisit?Id="+id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function CreateAnalysesDetails(id) {

    var Url = "../ManageAnalysesRequest/CreateAnalysesDetails?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteVisit(id) {
    var Url = "../ManageAnalysesRequest/DeleteVisit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function AddEng(id) {

    var Url = "../ManageAnalysesRequest/AddEng?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function DeleteVisitRow(id) {

    var Url = "../ManageAnalysesRequest/DeleteVisitRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            window.location = "../ManageAnalysesRequest/Index1";
        }
        else
            toastr.error(res[1]);
    });
}


function DeleteAnalyses(id) {
    var Url = "../ManageAnalysesRequest/DeleteVisitRow?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteFile(id) {
    var Url = "../ManageAnalysesRequest/DeleteFile?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}



function DeleteFileRow(id) {

    var Url = "../ManageAnalysesRequest/DeleteFileRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            window.location = "../ManageAnalysesRequest/Index1";
        }
        else
            toastr.error(res[1]);
    });
}


function DeleteAnalyses(id) {
    var Url = "../ManageAnalysesRequest/DeleteVisitRow?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function DeleteAnalysesRow(id) {

    var Url = "../ManageAnalysesRequest/DeleteAnalysesRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            window.location = "../ManageAnalysesRequest/Index1";
        }
        else
            toastr.error(res[1]);
    });
}


function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageAnalysesRequest/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}

function GetContact(id) {
    var Url = "../ManageAnalysesRequest/GetContact?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function GetServices(id) {
    var Url = "../ManageAnalysesRequest/GetServices?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function GetContract(id) {
    var Url = "../ManageAnalysesRequest/GetContract?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}




