﻿var ManageRequestOfferContract = {
    init: function () {
        Search();
    },
}
function Save() {

    var $form = $("#ModelForm");
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                //window.location = "../ManageRequestContract/Index";
                toastr.success(res[1]);
                Search();
            }
            else if (res[0] == "AddCondition")
            {
                toastr.success("تم اضافة الشرط بنجاح");
                location.reload();

            }
            else
                toastr.error(res[1]);
        });
        //  }
    }
}

function Create() {
    var Url = "../ManageRequestContract/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}




function Edit(id) {
    var Url = "../ManageRequestContract/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../ManageRequestContract/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../ManageRequestContract/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageRequestContract/Index";
        }
        else
            toastr.error(res[1]);
    });
}

function DeleteCondition(id) {
    var Url = "../ManageRequestContract/DeleteCondition?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}



function DeleteConditionRow(id) {

    var Url = "../ManageRequestContract/DeleteConditionRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            window.location = "../ManageRequestContract/Index1";
        }
        else
            toastr.error(res[1]);
    });
}


function GetContact(id) {
    var Url = "../ManageRequestContract/GetContact?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function GetServices(id) {
    var Url = "../ManageRequestOffer/GetServices?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageRequestContract/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}



