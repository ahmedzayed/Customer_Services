﻿using Customers.Abstracts;
using Customers.Abstracts.Marketing;
using Customers.Model.Marketing;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Marketing.Controllers
{
    [CustomAuthorize]

    public class ManageRequestController : BaseController
    {
        #region Service

        private readonly IRequestService _thisService;
        private readonly IRequestServicesService _RequestServicesService;
        private readonly CommonService _commonService;
        private readonly IRequestOfferService _RequestOfferservice;
        private readonly IRequestStatusService _RequestStatusServices;
        #endregion


        #region Constructor
        public ManageRequestController(IRequestService thisService, IRequestOfferService RequestOfferService,
            ILookupService lookupService,IRequestServicesService RequestServicesService,IRequestStatusService RequestStatusServices)
        {
            _thisService = thisService;
            _RequestOfferservice = RequestOfferService;
            _commonService = new CommonService(lookupService);
            _RequestServicesService = RequestServicesService;
            _RequestStatusServices = RequestStatusServices;
            //_CommunicationOfferSerive = CommunicationOfferSerive;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(MarketingSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            MarketingVm Obj = new MarketingVm();

            ViewBag.ActivityId = _commonService.FindCustomerActivities();
            ViewBag.EmployeeId = _commonService.FindEmployees();
            ViewBag.CustId = _commonService.FindCustomers();
            ViewBag.RequestStatusId = _commonService.FindCustomerStatus();

            
            


            return PartialView("~/Areas/Marketing/Views/ManageRequest/Create.cshtml", Obj);
        }




        [HttpPost]
        public JsonResult Create(MarketingVm viewModel, int? ServiceId)
        {
            if(viewModel.Id != 0)
            {

                if (ServiceId != 0)
                {

                    RequestsServicesVm RS = new RequestsServicesVm();
                   
                    RS.RequestId = viewModel.Id;
                    RS.ServiceId = ServiceId ??0;
                    _RequestServicesService.Save(RS);
                    if (_thisService.Save(viewModel))
                    {

                        return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


                }
                else
                {

                    if (_thisService.Save(viewModel))
                    {

                        return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
                }
            }

           
            else
            {
                if (ServiceId != 0)
                {
                    if (_thisService.Save(viewModel))
                    {
                        RequestsServicesVm RS = new RequestsServicesVm();

                        RS.RequestId = _RequestServicesService.FindLast();
                        RS.ServiceId = ServiceId ?? 0;
                        _RequestServicesService.Save(RS);

                        return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);


                }
                else
                {

                    if (_thisService.Save(viewModel))
                    {

                        return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            
        }


        public ActionResult Edit(int id = 0)
        {
            MarketingVm obj = _thisService.GetById(id);
            ViewBag.ActivityId = _commonService.FindCustomerActivities((int)obj.ActivityId);
            ViewBag.EmployeeId = _commonService.FindEmployees((int)obj.EmployeeId);
            ViewBag.CustId = _commonService.FindCustomers((int)obj.CustId);
            ViewBag.RequestStatusId = _commonService.FindCustomerStatus((int)obj.RequestStatusId);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Marketing/Views/ManageRequest/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            MarketingVm obj = new MarketingVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequest/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region StatusAction

        public ActionResult AddStatus(int Id)
        {
            ViewBag.RequestId = Id;
            return View();
        }
        [HttpPost]
        public JsonResult AddStatus(RequestStatusVm viewmodel)
        {
            viewmodel.CreatedBy = 0;
            viewmodel.CreatedDate = DateTime.Now;
            
            if (_RequestStatusServices.Save(viewmodel))
            {
                var Data = _thisService.GetById(viewmodel.RequestId);
                Data.RequestStatusId = viewmodel.StatusId;
                _thisService.Save(Data);
                return Json("AddStatus," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult ChangeStatas(int id = 0)
        {
            RequestStatusVm obj =  new RequestStatusVm();
            obj.RequestId = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequest/ChangeStatus.cshtml", obj);

            
        }
        [HttpPost]
        public JsonResult ChangeStatus(RequestStatusVm viewModel, int StatusId)
        {
            if (_RequestStatusServices.Save(viewModel))
            {
                var Data = _thisService.GetById(viewModel.RequestId);
                Data.RequestStatusId = StatusId;
                _thisService.Save(Data);
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);



        }
        public ActionResult GetStatus()
        {
            ViewBag.StatusId = _commonService.FindCustomerStatus();

            return PartialView("_Status");
        }

        public ActionResult GetStatusList(int? RequestId)
        {
            RequestStatusSm searchModel = new RequestStatusSm();


            var Data = _RequestStatusServices.GetServicesByRequestId(RequestId);

            return PartialView("_GetStatusList", Data);
        }
        
        [HttpGet]
        public ActionResult DeleteStatus(int id)
        {
            RequestStatusVm obj = new RequestStatusVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequest/DeleteStatus.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteStatusRow(int Id)
        {
            if (_RequestStatusServices.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region ServicesAction


        public ActionResult GetServices()
        {
            ViewBag.ServiceId = _commonService.FindServices();

            return PartialView("_Services");
        }


        public ActionResult AddServices(int Id)
        {
            ViewBag.RequestId = Id;
            return View();
        }
        [HttpPost]
        public JsonResult AddServices(RequestsServicesVm viewmodel)
        {
            viewmodel.CreatedBy = 0;
            viewmodel.CreatedDate = DateTime.Now;

            if (_RequestServicesService.Save(viewmodel))
            {
               
                return Json("AddServices," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetServicesList(int? RequestId)
        {
            RequestsServicesSm searchModel = new RequestsServicesSm();


            var Data = _RequestServicesService.GetServicesByRequestId(RequestId);

            return PartialView("_GetServicesList", Data);
        }
        public ActionResult DeleteServices(int id)
        {
            RequestsServicesVm obj = new RequestsServicesVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequest/DeleteServices.cshtml", obj);
        }


        public JsonResult DeleteServicesRow(int Id)
        {

            if (_RequestServicesService.Delete(Id))
            {



                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region RequestOffers
        [HttpPost]
        public JsonResult CreateOffer(int RequestId)
        {
            RequestOfferVm RO = new RequestOfferVm();
            RO.RequestId = RequestId;
            RO.OfferSelected = false;
            RO.CreatedDate = DateTime.Now;
            
           
           
            if (_RequestOfferservice.Save(RO))
            {
                return Json("success," + GlobalRes.MessageChange, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);

        }

        
        #endregion


    }
}