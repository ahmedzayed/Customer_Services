﻿using Customers.Abstracts;
using Customers.Abstracts.BasicInput;
using Customers.Abstracts.Marketing;
using Customers.Model.Marketing;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Marketing.Controllers
{
    [CustomAuthorize]

    public class ManageRequestOfferController : BaseController
    {
        #region Service

        private readonly IRequestOfferService _thisService;
        private readonly IRequestService _RequestService;
        private readonly ICommunicationOfficerService _CommService;
        private readonly IRequestServicesService _RequestServicesService;
        private readonly CommonService _commonService;
        private readonly IRequestOfferConditionService _RequestOfferConditionService;
        private readonly IRequestContractService _RequestContractService;

        #endregion


        #region Constructor
        public ManageRequestOfferController(IRequestOfferService thisService,IRequestService RequestService, IRequestServicesService RequestServicesService, ICommunicationOfficerService CommService, IRequestOfferConditionService RequestOfferConditionService,IRequestContractService RequestContractService,
            ILookupService lookupService)
        {
            _thisService = thisService;
            _RequestServicesService = RequestServicesService;
            _RequestService = RequestService;
            _RequestOfferConditionService = RequestOfferConditionService;
            _CommService = CommService;
            _RequestContractService = RequestContractService;

            _commonService = new CommonService(lookupService);

        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(RequestOfferSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            RequestOfferVm Obj = new RequestOfferVm();
            
            return PartialView("~/Areas/Marketing/Views/ManageRequestOffer/Create.cshtml", Obj);
        }

        [HttpPost]
        public JsonResult Create(RequestOfferVm viewModel )
        {
           
                viewModel.OfferSelected = true;
                if (_thisService.Save(viewModel))
                {
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            
           

        }

        public ActionResult Edit(int id = 0)
        {
            RequestOfferVm obj = _thisService.GetById(id);
          
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Marketing/Views/ManageRequestOffer/Create.cshtml", obj);
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            RequestOfferVm obj = new RequestOfferVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequestOffer/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Offers

        public ActionResult GetOffers(int? OfferId)
        {
            if( OfferId !=null)
            {
                ViewBag.OfferId = _commonService.FindOffer((int)OfferId);


            }
            else
            ViewBag.OfferId = _commonService.FindOffer();

            return PartialView("_Offer");
        }




        #endregion


        #region Conditions


        public ActionResult AddCondition(int Id)
        {
            ViewBag.RequestId = Id;
            return View();
        }
        [HttpPost]
        public JsonResult AddCondition(RequestOfferConditionVm viewmodel)
        {
            viewmodel.CreatedBy = 0;
            viewmodel.CreatedDate = DateTime.Now;
           
            if (_RequestOfferConditionService.Save(viewmodel))
            {
                return Json("AddCondition," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult DeleteCondition(int id)
        {
            RequestOfferConditionVm obj = new RequestOfferConditionVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequestOffer/DeleteCondition.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteConditionRow(int Id)
        {
            if (_RequestOfferConditionService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCondition()
        {
            
                ViewBag.ConditionId = _commonService.FindConditions();

            return PartialView("_Conditions");
        }


        public ActionResult GetOfferConditionList(int? RequestOfferId)
        {
            RequestOfferConditionSm searchModel = new RequestOfferConditionSm();


            var Data = _RequestOfferConditionService.GetByRequestOfferId(RequestOfferId);

            return PartialView("_GetConditonsList", Data);
        }

        #endregion


        #region ChangeToContract
        public JsonResult ChangeToContract(int RequestOfferId)
        {
            RequestOfferContractVm RO = new RequestOfferContractVm();
            RO.RequestOfferId = RequestOfferId;
            
            RO.CreatedDate = DateTime.Now;



            if (_RequestContractService.Save(RO))
            {
                return Json("success," + GlobalRes.MessageChange, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GetContact(int Id)
        {
            int Custid = _RequestService.GetById(Id).CustId;
          var Obj=  _CommService.GetByCustomerId(Custid);
           
            return PartialView("GetContact",Obj);
        }
        
        public ActionResult GetServices(int Id)
        {
           
            var Obj = _RequestServicesService.GetServicesByRequestId(Id);

            return PartialView("GetServices", Obj);
        }

        #endregion
    }
}