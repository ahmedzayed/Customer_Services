﻿using Customers.Abstracts;
using Customers.Abstracts.BasicInput;
using Customers.Abstracts.Marketing;
using Customers.Model.Marketing;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Marketing.Controllers
{
    [CustomAuthorize]
    public class ManageRequestContractController : BaseController
    {
        #region Services

        private readonly IRequestContractService _thisService;
        private readonly IRequestOfferContractService _RequestOfferContractService;
        private readonly CommonService _commonService;
        private readonly ICommunicationOfficerService _CommService;

        #endregion

        #region Constructor
        public ManageRequestContractController(IRequestContractService thisService,ICommunicationOfficerService CommService,ILookupService lookupService,IRequestOfferContractService RequestOfferContractService)
        {
            _thisService = thisService;
            _CommService = CommService;
            
            _RequestOfferContractService = RequestOfferContractService;
            _commonService = new CommonService(lookupService);
        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(RequestOfferContractSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            RequestOfferContractVm Obj = new RequestOfferContractVm();
            return PartialView("~/Areas/BasicInput/Views/ManageRequestContract/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(RequestOfferContractVm viewModel ,int? ConditionId)
        {
            if (ConditionId > 0)
            {
                RequestsOffersContractsConditionsVm V = new RequestsOffersContractsConditionsVm();
                V.ConditionId = ConditionId;
                V.OfferContractId = viewModel.Id;
               
               
                
                if (_RequestOfferContractService.Save(V))
                {
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }

        
            else {
               


                if (_thisService.Save(viewModel))
                {
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
                }

        }
        public ActionResult Edit(int id = 0)
        {
            RequestOfferContractVm obj = _thisService.GetById(id);
            ViewBag.ContractId = _commonService.FindContract((int)obj.ContractId);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Marketing/Views/ManageRequestContract/Create.cshtml", obj);
        }

        //public ActionResult Details(int id)
        //{
        //    return PartialView(_thisService.GetById(id));
        //}

        [HttpGet]
        public ActionResult Delete(int id)
        {
            RequestOfferContractVm obj = new RequestOfferContractVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequestContract/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Condition

        public ActionResult AddCondition(int Id)
        {
            ViewBag.RequestId = Id;
            return View();
        }
        [HttpPost]
        public JsonResult AddCondition(RequestsOffersContractsConditionsVm viewmodel)
        {
            viewmodel.CreatedBy = 0;
            viewmodel.CreatedDate = DateTime.Now;
            if (_RequestOfferContractService.Save(viewmodel))
            {
                return Json("AddCondition," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetConditionList(int? RequestOfferId)
        {
            RequestOfferContractSm searchModel = new RequestOfferContractSm();


            var Data = _RequestOfferContractService.GetByRequestOfferId(RequestOfferId);

            return PartialView("_GetConditionList", Data);
        }
        public ActionResult GetCondition()
        {

            ViewBag.ConditionId = _commonService.FindConditions();

            return PartialView("_Conditions");
        }
        [HttpGet]
        public ActionResult DeleteCondition(int id)
        {
            RequestsOffersContractsConditionsVm obj = new RequestsOffersContractsConditionsVm();
            obj.Id = id;
            return PartialView("~/Areas/Marketing/Views/ManageRequestContract/DeleteCondition.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteConditionRow(int Id)
        {
            if (_RequestOfferContractService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetContact(int Id)
        {
            //int Custid = _RequestService.GetById(Id).CustId;
            var Obj = _CommService.GetByCustomerId(Id);

            return PartialView("GetContact", Obj);
        }

        #endregion
    }
}