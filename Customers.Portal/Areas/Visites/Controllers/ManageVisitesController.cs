﻿using Customers.Abstracts.BasicInput;
using Customers.Abstracts.Marketing;
using Customers.Abstracts.Visites;
using Customers.Model.Marketing;
using Customers.Model.Visites;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.Visites.Controllers
{
    [CustomAuthorize]
    public class ManageVisitesController : BaseController
    {
        #region Services

        private readonly IVisitesService _thisService;
        private readonly IRequestService _RequestServices;
        private readonly ICommunicationOfficerService _CommService;
        private readonly IRequestServicesService _RequestServicesService;
        private readonly ICustomerService _customerService;


        #endregion

        #region Constructor
        public ManageVisitesController(IVisitesService thisService, IRequestService RequestService,ICustomerService CustomerService, ICommunicationOfficerService CommunicationOfficerService, IRequestServicesService RequestServicesService )
        {
            _RequestServices = RequestService;
            _RequestServicesService = RequestServicesService;
            _customerService = CustomerService;
            _thisService = thisService;
            _CommService = CommunicationOfficerService;
        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(MarketingSm searchModel)
        {
            int EmpId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (Customers.Helpers.Utilities.ResourcesReader.ISAdmin == true)
            {
                var result = _RequestServices.FindAll(searchModel);
                return PartialView(result);
            }
            else
            {
                var result = _RequestServices.GetByEmployeeId(EmpId);
                return PartialView(result);
            }
        }

        public ActionResult Create()
        {
            VisitVm Obj = new VisitVm();
            return PartialView("~/Areas/Visites/Views/ManageVisites/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(VisitVm viewModel)
        {
           
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            VisitVm obj = _thisService.GetById(id);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Visites/Views/ManageVisites/Create.cshtml", obj);
        }

        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            VisitVm obj = new VisitVm();
            obj.Id = id;
            return PartialView("~/Areas/Visites/Views/ManageVisites/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion


        public ActionResult GetContact(int Id)
        {
            var Obj = _CommService.GetByCustomerId(Id);

            return PartialView("GetContact", Obj);
        }

        public ActionResult GetServices(int Id)
        {

            var Obj = _RequestServicesService.GetServicesByRequestId(Id);
           

            return PartialView("GetServices", Obj);
        }

        public ActionResult AddVisit(int CustomerId)
        {
            ViewBag.CustomerId = CustomerId;
            ViewBag.CustomerName = _customerService.GetById(CustomerId).NameAr;
             return View();
        }
        [HttpPost]
        public ActionResult AddVisit(VisitVm viewmodel)
        {
            viewmodel.CreatedDate = DateTime.Now;
            viewmodel.VisitDate = DateTime.Now;
            viewmodel.EmployeeId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            if (_thisService.Save(viewmodel))
            {
               
                return Json("AddStatus," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVisit(int CustomerId)
        {
            VisitVm searchModel = new VisitVm();


            var Data = _thisService.GetByCustomerId(CustomerId);

            return PartialView("_GetVisites", Data);
        }
    }
}