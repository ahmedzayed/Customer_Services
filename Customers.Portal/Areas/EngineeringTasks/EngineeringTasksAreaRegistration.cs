﻿using System.Web.Mvc;

namespace Customers.Portal.Areas.EngineeringTasks
{
    public class EngineeringTasksAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EngineeringTasks";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EngineeringTasks_default",
                "EngineeringTasks/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}