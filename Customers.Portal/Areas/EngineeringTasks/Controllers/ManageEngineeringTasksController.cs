﻿using Customers.Abstracts;
using Customers.Abstracts.EngineeringTasks;
using Customers.Model.EngineeringTask;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.EngineeringTasks.Controllers
{
    [CustomAuthorize]

    public class ManageEngineeringTasksController : BaseController
    {
        #region Services

        private readonly IEngineeringTasksService _thisService;
        private readonly CommonService _commonService;


        #endregion

        #region Constructor
        public ManageEngineeringTasksController(IEngineeringTasksService thisService, ILookupService lookupService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);

        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult EnginerringTasks()
        {
            return View();
        }
        public ActionResult Taskes()
        {
            int EmployeeId = Customers.Helpers.Utilities.ResourcesReader.UserId;
            bool? IsAdmin = Customers.Helpers.Utilities.ResourcesReader.ISAdmin;
            if (IsAdmin == true)
            {
                EngineeringTaskSm searchModel = new EngineeringTaskSm();
                var result1 = _thisService.FindAll(searchModel);
                return PartialView("_Taskes",result1);
            }
            else
            {
                var result = _thisService.GetByEmployeeId(EmployeeId);
                return PartialView("_Taskes",result);
            }
        }

        public ActionResult Search(EngineeringTaskSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            EngineeringTaskVm Obj = new EngineeringTaskVm();
            ViewBag.EmployeeId = _commonService.FindEngineeringEmp();
            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(EngineeringTaskVm viewModel)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploaded/EnginneringTasks/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    viewModel.OrderFile = fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
            viewModel.OrderCase = 0;
            viewModel.OrderChangeDate = DateTime.Now;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            EngineeringTaskVm obj = _thisService.GetById(id);
            ViewBag.EmployeeId = _commonService.FindEngineeringEmp(obj.EmployeeId);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/Create.cshtml", obj);
        }
        public ActionResult AcceptOrder(int id = 0)
        {
            EngineeringTaskVm obj = _thisService.GetById(id);
            ViewBag.EmployeeId = _commonService.FindEngineeringEmp(obj.EmployeeId);
            obj.OrderCase = 1;

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/AcceptOrder.cshtml", obj);
        }
        [HttpPost]
        public ActionResult AcceptOrder(EngineeringTaskVm viewModel)
        {
            viewModel.OrderCase = 1;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DelayOrder(int id = 0)
        {
            EngineeringTaskVm obj = _thisService.GetById(id);
            ViewBag.EmployeeId = _commonService.FindEngineeringEmp(obj.EmployeeId);
            obj.OrderCase = 2;

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/DelayOrder.cshtml", obj);
        }
        [HttpPost]
        public ActionResult DelayOrder(EngineeringTaskVm viewModel)
        {
            viewModel.OrderCase = 2;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult FinshedOrder(int id = 0)
        {
            EngineeringTaskVm obj = _thisService.GetById(id);
            ViewBag.EmployeeId = _commonService.FindEngineeringEmp(obj.EmployeeId);
            obj.OrderCase = 3;

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/FinshedOrder.cshtml", obj);
        }
        [HttpPost]
        public ActionResult FinshedOrder(EngineeringTaskVm viewModel)
        {
            viewModel.OrderCase = 3;
            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EngineeringTaskVm obj = new EngineeringTaskVm();
            obj.Id = id;

            return PartialView("~/Areas/EngineeringTasks/Views/ManageEngineeringTasks/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}