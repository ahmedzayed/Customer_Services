﻿var ManageContracts = {
    init: function () {
        Search();
    },
}
function Save() {

    var $form = $("#ModelForm");
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                //window.location = "../ManageContracts/Index";
                toastr.success(res[1]);
                Search();
            }
            else if (res[0] == "SuEditItems") {
            
                toastr.success("تم  اضافه البند بنجاح");

                location.reload();

            }
            else
                toastr.error(res[1]);
        });
        //  }
    }
}
function Create() {
    var Url = "../ManageContracts/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
//function AddItems(id)
//{
//    var Url = "../ManageContracts/AddItems?Id=" + id;
//    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
//        $('#myModalAddEdit').modal('show');
//    });
//}
function Edit(id) {
    var Url = "../ManageContracts/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../ManageContracts/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../ManageContracts/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageContracts/Index";
        }
        else
            toastr.error(res[1]);
    });
}




function DeleteItems(id) {
    var Url = "../../ManageContracts/DeleteItems?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function DeleteItemsRow(id) {

    var Url = "../../ManageContracts/DeleteItemsRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            location.reload();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageContracts/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


