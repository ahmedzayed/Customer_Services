﻿using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    //[CustomAuthorize]

    public class ManageServicesController : BaseController
    {
        #region Services

        private readonly IServicesService _thisService;

        #endregion

        #region Constructor
        public ManageServicesController(IServicesService thisService)
        {
            _thisService = thisService;
        }
        #endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(ServicesSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            ServicesVm Obj = new ServicesVm();
            return PartialView("~/Areas/BasicInput/Views/ManageServices/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(ServicesVm viewModel)
        {
            if (viewModel.ServiceNameEn == null || viewModel.ServiceNameEn == "")
                viewModel.ServiceNameEn = viewModel.ServiceNameEn;

            if (_thisService.Save(viewModel))
            {
                return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            ServicesVm obj = _thisService.GetById(id);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageServices/Create.cshtml", obj);
        }

        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ServicesVm obj = new ServicesVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageServices/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}