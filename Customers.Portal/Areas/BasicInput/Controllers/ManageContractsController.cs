﻿using Customers.Abstracts;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    //[CustomAuthorize]

    public class ManageContractsController : BaseController
    {
        #region Service

        private readonly IContractsService _thisService;
        private readonly IContractsItemsService _ContractsItemsService;
        private readonly CommonService _commonService;
        #endregion


        #region Constructor
        public ManageContractsController(IContractsService thisService, ILookupService lookupService, IContractsItemsService ContractsItemsService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);
            _ContractsItemsService = ContractsItemsService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(BasicInputSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            BasicInputVm Obj = new BasicInputVm();

            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Create.cshtml", Obj);
        }


        public ActionResult GetItems()
        {
            ViewBag.ItemsId = _commonService.FindItems();

            return PartialView("_Items");
        }


        [HttpPost]
        public JsonResult Create(BasicInputVm viewModel )
        {

            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;


          
                if (_thisService.Save(viewModel))
                {
                   
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            
        }



        public ActionResult Edit(int id = 0)
        {
            BasicInputVm obj = _thisService.GetById(id);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BasicInputVm obj = new BasicInputVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Items
        public ActionResult GetItemsList(int? Id)
        {
            ContractsItemsSm searchModel = new ContractsItemsSm();


            var Data = _ContractsItemsService.GetByContractsId(Id);

            return PartialView("_GetItemsList", Data);
        }

        public ActionResult AddItems(int Id)
        {
            ViewBag.ContractId = Id;
            ViewBag.ContractName = _thisService.GetById(Id).NameAr;

           
            //return PartialView("~/Areas/BasicInput/Views/ManageContracts/AddItems.cshtml", obj);
            return View("AddItems");
        }
        [HttpPost]
        public JsonResult AddItems(int? ItemsId,int ContractId)
        {
            ContractsItemsVm C = new ContractsItemsVm();
            C.ContractsId = ContractId /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
            C.ItemsId = ItemsId;
           if( _ContractsItemsService.Save(C))
            return Json("SuEditItems," + GlobalRes.MessageSuccuss.ToString() , JsonRequestBehavior.AllowGet);
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);


            //return PartialView("~/Areas/BasicInput/Views/ManageContracts/AddItems.cshtml", obj);
        }
        public ActionResult DeleteItems(int id)
        {
            ContractsItemsVm obj = new ContractsItemsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_ContractsItemsService.Delete(Id))
            {



                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}