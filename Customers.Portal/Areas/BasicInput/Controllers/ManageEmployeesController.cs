﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Customers.Portal.Controllers;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Resources.GlobalRes;
using Customers.Abstracts.Administration;
using Customers.Model.Administration;
using Customers.Portal.Utilities;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    //[CustomAuthorize]

    public class ManageEmployeesController : BaseController
    {
        #region Service

        private readonly IEmployeesService _thisService;
        private readonly IEmployeesConditionService _EmployeesConditionService;
        private readonly IWorkContractService _WorkContractService;
        private readonly Utilities.CommonService _commonService;
        private readonly IAdministrationServiec _AdminService;
        #endregion


        #region Constructor
        public ManageEmployeesController(IEmployeesService thisService, IWorkContractService WorkContractService, IAdministrationServiec AdminService, Abstracts.ILookupService lookupService, IEmployeesConditionService EmployeesConditionService)
        {
            _thisService = thisService;
            _commonService = new Utilities.CommonService(lookupService);
            _EmployeesConditionService = EmployeesConditionService;
            _WorkContractService = WorkContractService;
            _AdminService = AdminService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(EmployeesSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }


       
        public ActionResult Create()
        {
            EmployeesVm Obj = new EmployeesVm();
            ViewBag.SpecializationsId = _commonService.FindSpecialization();
            ViewBag.WorkSystemId = _commonService.FindWorkSystem();
            ViewBag.EmployeeTypeId = _commonService.FindEmployeeType();
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Create.cshtml", Obj);
        }




        [HttpPost]
        public JsonResult Create(EmployeesVm viewModel)
        {
            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;

           

                if (_thisService.Save(viewModel))
            {
                AuthUserVm User = new AuthUserVm();
                User.UserName = viewModel.UserName;
                User.Password = viewModel.Password;
                User.Name = viewModel.NameAr;
                User.IsActive = true;
                User.TypeId = 1;
                EmployeesSm S = new EmployeesSm();
                User.UserId = _thisService.FindAll(S).Select(a=>a.Id).LastOrDefault();

                _AdminService.Save(User);

                return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + Resources.GlobalRes.GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            



        }


        public ActionResult Edit(int id = 0)
        {
            EmployeesVm obj = _thisService.GetById(id);
            ViewBag.SpecializationsId = _commonService.FindSpecialization(obj.SpecializationsId);
            ViewBag.WorkSystemId = _commonService.FindWorkSystem(obj.WorkSystemId);
            ViewBag.EmployeeTypeId = _commonService.FindEmployeeType(obj.EmployeeTypeId);

            //ViewBag.CustomerActivityId = _commonService.FindCustomerActivities(obj.CustomerActivityId);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EmployeesVm obj = new EmployeesVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region WorkContract

        public ActionResult AddWorkContract(int Id)
        {
            ViewBag.EmployeeId = Id;
            ViewBag.EmployeeName = _thisService.GetById(Id).NameAr;
            return View();
        }

        [HttpPost]
        public JsonResult AddWorkContract(WorkContractVm viewmodel)
        {
            //WorkContractVm C = new WorkContractVm();
            //C.EndDate = EndDate /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
            //C.EmployeeId = EmployeeId;
            //C.StartDate = StartDate;
            viewmodel.CreatedBy = 0;
            viewmodel.CreatedDate = DateTime.Now;
            viewmodel.UpdatedBy = 0;
            viewmodel.UpdatedDate = DateTime.Now;
            if (_WorkContractService.Save(viewmodel))
                return Json("SuEditItems," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);


            //return PartialView("~/Areas/BasicInput/Views/ManageContracts/AddItems.cshtml", obj);
        }
        public ActionResult GetWorkContract(int? Id)
        {

            WorkContractSm searchModel = new WorkContractSm();


            var Data = _WorkContractService.GetByEmployeeId(Id);

            return PartialView("_GetWorkContractList", Data);
        }
        //public ActionResult AddWorkContract(int id)
        //{
        //    WorkContractVm obj = new WorkContractVm();
        //    obj.EmployeeId = id;
        //    return PartialView("~/Areas/BasicInput/Views/ManageEmployees/CreateWorkContract.cshtml", obj);


        //}

        //[HttpPost]
        //public JsonResult AddWorkContract(WorkContractVm viewModel)
        //{
        //    viewModel.Id = 0;

        //        if (_WorkContractService.Save(viewModel))
        //        {
        //            return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

        //        }
        //        else
        //            return Json("error," + Resources.GlobalRes.GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        //    }


        [HttpGet]
        public ActionResult DeleteContract(int id)
        {
            WorkContractVm obj = new WorkContractVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/DeleteContract.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteContractRow(int Id)
        {
            if (_WorkContractService.Delete(Id))
            {
                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Condions
        public ActionResult AddCondition(int Id)
        {
            ViewBag.EmployeeId = Id;
            ViewBag.EmployeeName = _thisService.GetById(Id).NameAr;
            return View();
        }

        [HttpPost]
        public JsonResult AddCondition(int CondionId, int EmployeeId)
        {
           EmployeesConditionsVm C = new EmployeesConditionsVm();
            C.CondionId = CondionId /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
            C.EmployeeId = EmployeeId;
            if (_EmployeesConditionService.Save(C))
                return Json("SuEditItems," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);


            //return PartialView("~/Areas/BasicInput/Views/ManageContracts/AddItems.cshtml", obj);
        }
        public ActionResult GetConditionsEmp()
        {
            ViewBag.CondionId = _commonService.FindConditionsEmp();

            return PartialView("_Conditions");
        }


        public ActionResult GetCondionsEmpList(int? Id)
        {
            EmployeesConditionsSm searchModel = new EmployeesConditionsSm();


            var Data = _EmployeesConditionService.GetByEmployeeId(Id);

            return PartialView("_GetConditionsEmpList", Data);
        }


      

        public ActionResult DeleteItems(int id)
        {
            EmployeesConditionsVm obj = new EmployeesConditionsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_EmployeesConditionService.Delete(Id))
            {



                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}