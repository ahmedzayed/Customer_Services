﻿using Customers.Abstracts;
using Customers.Abstracts.Administration;
using Customers.Abstracts.BasicInput;
using Customers.Model.Administration;
using Customers.Model.BasicInput;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    //[CustomAuthorize]

    public class ManageCustomersController : BaseController
    {
        #region Service

        private readonly ICustomerService _thisService;
        private readonly ICommunicationOfficerService _CommunicationOfferSerive;
        private readonly CommonService _commonService;
        private readonly IAdministrationServiec _AdminService;
        #endregion


        #region Constructor
        public ManageCustomersController(ICustomerService thisService,IAdministrationServiec AdminService,
            ILookupService lookupService,ICommunicationOfficerService CommunicationOfferSerive)
        {
            _thisService = thisService;
            _AdminService = AdminService;
            _commonService = new CommonService(lookupService);
            _CommunicationOfferSerive = CommunicationOfferSerive;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(CustomerSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            CustomerVm Obj = new CustomerVm();
            ViewBag.CustomerActivityId = _commonService.FindCustomerActivities();
            //return PartialView("~/Areas/BasicInput/Views/ManageCustomers/Create.cshtml", Obj);
            return View(Obj);
        }

      

      
        [HttpPost]
        public JsonResult Create(CustomerVm viewModel,string PhoneNumber, string JobName,string Email,string CommName ,int? CommId)
        {
            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;

            if (viewModel.Id == 0)
            {
                _thisService.Save(viewModel);
                AuthUserVm User = new AuthUserVm();
                User.UserName = viewModel.UserName;
                User.Password = viewModel.Password;
                User.Name = viewModel.NameAr;
                User.IsActive = true;
                User.TypeId = 2;
                User.UserId = _thisService.FindLast();
            
                _AdminService.Save(User);
                CommunicationOfficerVm Cmof = new CommunicationOfficerVm();
                Cmof.Name = CommName;
                Cmof.JobeName = JobName;
                Cmof.Email = Email;
                Cmof.PhoneNumber = PhoneNumber;
                Cmof.CustomerId = _thisService.FindLast();
                var Id = 0;

                if ( _CommunicationOfferSerive.Save(Cmof))
                {
                
                    return Json("success," +Id , JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {

                if (CommName != "")
                {
                    CommunicationOfficerVm Cmof = new CommunicationOfficerVm();
                    Cmof.Name = CommName;
                    Cmof.JobeName = JobName;
                    Cmof.Email = Email;
                    if (CommId != null)
                    Cmof.Id =(int) CommId;
                    Cmof.PhoneNumber = PhoneNumber;
                    Cmof.CustomerId = viewModel.Id;
                    if (_CommunicationOfferSerive.Save(Cmof))
                    {
                        var Id = viewModel.Id;
                        
                        return Json("success," + Id, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

                    }
                }
                else
                     if (_thisService.Save(viewModel))
                {
                    _thisService.Save(viewModel);
                    AuthUserVm User = new AuthUserVm();
                    User.UserName = viewModel.UserName;
                    User.Password = viewModel.Password;
                    User.Name = viewModel.NameAr;
                    User.IsActive = true;
                    User.TypeId = 2;
                    User.UserId = User.UserId = _thisService.FindLast();
                    

                    _AdminService.Save(User);
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

                }
            }
        }


        public ActionResult Edit(int id = 0)
        {
            CustomerVm obj = _thisService.GetById(id);
            ViewBag.CustomerActivityId = _commonService.FindCustomerActivities(obj.CustomerActivityId);

            if (obj == null)
            {
                return HttpNotFound();
            }
            //return PartialView("~/Areas/BasicInput/Views/ManageCustomers/Create.cshtml", obj);
            return View("Create",obj);

        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            CustomerVm obj = new CustomerVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageCustomers/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// / CommunicationOfficer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        #region CommunicationOfficer Action

      
        [HttpGet]
        public ActionResult CommunicationOfficer(int? Id)
        {
            CommunicationOfficerSm searchModel = new CommunicationOfficerSm();


            var Data = _CommunicationOfferSerive.GetByCustomerId(Id);

            return PartialView("_CommunicationOfficer", Data);
        }

        public ActionResult DeleteComm(int id)
        {
            CommunicationOfficerVm obj = new CommunicationOfficerVm();
            obj.Id = id;
        
            return PartialView("~/Areas/BasicInput/Views/ManageCustomers/DeleteComm.cshtml", obj);
        }


        public JsonResult DeleteCommRow(int Id)
        {
            
            if (_CommunicationOfferSerive.Delete(Id))
            {
             
               

                return Json("success," + GlobalRes.Messagedelete,  JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}