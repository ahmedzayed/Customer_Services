﻿using Customers.Abstracts.Marketing;
using Customers.Portal.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class SharedController : BaseController
    {
        // GET: BasicInput/Shared

        private readonly IRequestOfferService _RequestService;
            public SharedController(IRequestOfferService RequestService)
        {
            _RequestService = RequestService;
        }
        public ActionResult _SideMenu()
        {
            ViewBag.Marketing=_RequestService.Count();
            return PartialView();
        }
    }
}