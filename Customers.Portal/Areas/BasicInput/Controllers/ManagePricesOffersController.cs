﻿using Customers.Abstracts;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;

using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    //[CustomAuthorize]

    public class ManagePricesOffersController : BaseController
    {
        #region Service

        private readonly IPriceOffersService _thisService;
        private readonly IPriceOffersConditionsService _PriceOffersConditionsService;
        private readonly CommonService _commonService;
        #endregion


        #region Constructor
        public ManagePricesOffersController(IPriceOffersService thisService, ILookupService lookupService, IPriceOffersConditionsService PriceOffersConditionsService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);
            _PriceOffersConditionsService = PriceOffersConditionsService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(BasicInputSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            BasicInputVm Obj = new BasicInputVm();

            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Create.cshtml", Obj);
        }


        public ActionResult GetConditions()
        {
            ViewBag.ConditionsId = _commonService.FindConditions();

            return PartialView("_Conditions");
        }


        [HttpPost]
        public JsonResult Create(BasicInputVm viewModel)
        {

            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;


          
                if (_thisService.Save(viewModel))
                {
                    
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            
        }



        public ActionResult Edit(int id = 0)
        {
            BasicInputVm obj = _thisService.GetById(id);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BasicInputVm obj = new BasicInputVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Condition
        public ActionResult AddCondition(int Id)
        {
            ViewBag.PriceOfferId = Id;
            ViewBag.PricOfferName = _thisService.GetById(Id).NameAr;
            return View();
        }

        [HttpPost]
        public JsonResult AddCondition(int ConditionsId, int PriceOfferId)
        {
            PriceOffersConditionVm C = new PriceOffersConditionVm();
            C.ConditionsId = ConditionsId /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
            C.PriceOffersId = PriceOfferId;
            if (_PriceOffersConditionsService.Save(C))
                return Json("SuEditItems," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);


            //return PartialView("~/Areas/BasicInput/Views/ManageContracts/AddItems.cshtml", obj);
        }
        public ActionResult GetCondionsList(int? Id)
        {
            //ContractsItemsSm searchModel = new ContractsItemsSm();


            var Data = _PriceOffersConditionsService.GetByPriceOffersId(Id);

            return PartialView("_GetConditionsList", Data);
        }


        public ActionResult DeleteItems(int id)
        {
            ContractsItemsVm obj = new ContractsItemsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_PriceOffersConditionsService.Delete(Id))
            {



                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}