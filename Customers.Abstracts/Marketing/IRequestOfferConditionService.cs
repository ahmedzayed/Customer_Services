﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
    public interface IRequestOfferConditionService
    {
        RequestOfferConditionVm GetById(int id);
        bool Save(RequestOfferConditionVm viewModel);
        List<RequestOfferConditionVm> FindAll(RequestOfferConditionSm searchModel);

        bool Delete(int id);
        List<RequestOfferConditionVm> GetByRequestOfferId(int? id);

    }
}
