﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
   public interface IRequestService
    {
        MarketingVm GetById(int id);

        List<MarketingVm> GetByEmployeeId(int EmployeeId);

        bool Save(MarketingVm viewModel);
        List<MarketingVm> FindAll(MarketingSm searchModel);

        int Count();


        bool Delete(int id);
    }
}
