﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
   public interface IRequestOfferService
    {
        RequestOfferVm GetById(int id);
        bool Save(RequestOfferVm viewModel);
        List<RequestOfferVm> FindAll(RequestOfferSm searchModel);
        int Count();
        bool Delete(int id);
    }
}
