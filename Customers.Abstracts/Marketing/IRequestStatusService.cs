﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
    public interface IRequestStatusService
    {
        RequestStatusVm GetById(int id);
        RequestStatusVm GetByRequestId(int id);


        List<RequestStatusVm> GetServicesByRequestId(int? id);

        bool Save(RequestStatusVm viewModel);
      
        List<RequestStatusVm> FindAll(RequestStatusSm searchModel);
        bool Delete(int id);
    }
}
