﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
   public interface IRequestContractService
    {
        RequestOfferContractVm GetById(int id);
        bool Save(RequestOfferContractVm viewModel);
        List<RequestOfferContractVm> FindAll(RequestOfferContractSm searchModel);
        List<RequestOfferContractVm> GetByCustomerId(int? id);

        int Count();
        bool Delete(int id);
    }
}
