﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
    public interface IRequestServicesService
    {
        RequestsServicesVm GetById(int id);

        List<RequestsServicesVm> GetServicesByRequestId(int? id);

        List<RequestsServicesVm> GetServicesByCustomerId(int? id);


        bool Save(RequestsServicesVm viewModel);
        int FindLast();

        List<RequestsServicesVm> FindAll(RequestsServicesSm searchModel);
        bool Delete(int id);
    }
}
