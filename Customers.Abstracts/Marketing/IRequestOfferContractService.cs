﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Marketing
{
    public interface IRequestOfferContractService
    {
        RequestsOffersContractsConditionsVm GetById(int id);
        bool Save(RequestsOffersContractsConditionsVm viewModel);
        List<RequestsOffersContractsConditionsVm> FindAll(RequestsOffersContractsConditionsSm searchModel);
        List<RequestsOffersContractsConditionsVm> GetByRequestOfferId(int? id);
        List<RequestsOffersContractsConditionsVm> GetByCustomerId(int? id);


        bool Delete(int id);
    }
}
