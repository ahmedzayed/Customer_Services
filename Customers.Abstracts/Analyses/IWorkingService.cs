﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Analyses
{
   public interface IWorkingService
    {
        WorkingVm GetById(int id);
         List<WorkingVm> GetWorkingList(int id,int RequestId);


        bool Save(WorkingVm viewModel);
        List<WorkingVm> FindAll(WorkingSm searchModel);

        int Count();


        bool Delete(int id);
    }
}
