﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Analyses
{
    public interface IAnalysesVisitService
    {
        AnalysesVisitsVm GetById(int id);

        bool Save(AnalysesVisitsVm viewModel);
        List<AnalysesVisitsVm> FindAll(AnalysesVisitsSm searchModel);
        List<AnalysesVisitsVm> GetByRequestId(int id);

        int Count();


        bool Delete(int id);
    }
}
