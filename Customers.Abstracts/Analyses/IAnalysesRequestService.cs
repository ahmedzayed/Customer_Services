﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Analyses
{
  public interface   IAnalysesRequestService
    {
        AnalysesRequestVm GetById(int id);
      List<AnalysesRequestVm> GetByCustomerId(int id);

        bool Save(AnalysesRequestVm viewModel);
        List<AnalysesRequestVm> FindAll(AnalysesRequestSm searchModel);

        int Count();


        bool Delete(int id);
    }
}
