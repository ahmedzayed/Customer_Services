﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Analyses
{
  public    interface   IAnalysesRequestDetailsService
    {
        AnalysesRequestDetailsVm GetById(int id);

        bool Save(AnalysesRequestDetailsVm viewModel);
        List<AnalysesRequestDetailsVm> FindAll(AnalysesRequestDetailsSm searchModel);

        List<AnalysesRequestDetailsVm> GetByRequestId(int id);

        int Count();


        bool Delete(int id);
    }
}
