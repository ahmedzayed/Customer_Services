﻿using Customers.Model.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Administration
{
 public interface    IAdministrationServiec
    {
        AuthUserVm CheckUser(string userName, string password);
        bool Save(AuthUserVm viewModel);
    }
}
