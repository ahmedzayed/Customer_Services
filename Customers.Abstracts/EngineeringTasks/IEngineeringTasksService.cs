﻿using Customers.Model.EngineeringTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.EngineeringTasks
{
    public interface IEngineeringTasksService
    {
        EngineeringTaskVm GetById(int id);
        bool Save(EngineeringTaskVm viewModel);
        List<EngineeringTaskVm> FindAll(EngineeringTaskSm searchModel);
        bool Delete(int id);
        List<EngineeringTaskVm> GetByEmployeeId(int EmployeeId);
    }

}
