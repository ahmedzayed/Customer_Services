﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
  public  interface ICustomerAnalysesService
    {
        BasicInputVm GetById(int id);
        bool Save(BasicInputVm viewModel);
        List<BasicInputVm> FindAll(BasicInputSm searchModel);
        bool Delete(int id);
        int FindLast();
    }
}
