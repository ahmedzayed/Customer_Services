﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
    public interface IEmployeesService
    {
        EmployeesVm GetById(int id);

        bool Save(EmployeesVm viewModel);
        List<EmployeesVm> FindAll(EmployeesSm searchModel);
        bool Delete(int id);
         int FindLast();
    }
}
