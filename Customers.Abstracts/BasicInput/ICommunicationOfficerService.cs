﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
   public interface ICommunicationOfficerService
    {

        CommunicationOfficerVm GetById(int id);

        bool Save(CommunicationOfficerVm viewModel);
        List<CommunicationOfficerVm> FindAll(CommunicationOfficerSm searchModel);
        bool Delete(int id);
        List<CommunicationOfficerVm> GetByCustomerId(int? id);

    }
}
