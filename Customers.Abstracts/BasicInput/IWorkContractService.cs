﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Model.BasicInput;

namespace Customers.Abstracts.BasicInput
{
    public interface IWorkContractService
    {
        WorkContractVm GetById(int id);

        bool Save(WorkContractVm viewModel);
        List<WorkContractVm> FindAll(WorkContractSm searchModel);
        bool Delete(int id);
        List<WorkContractVm> GetByEmployeeId(int? id);
    }
}
