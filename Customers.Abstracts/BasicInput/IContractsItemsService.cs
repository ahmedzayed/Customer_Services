﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
 public   interface IContractsItemsService
    {
        ContractsItemsVm GetById(int id);
        bool Save(ContractsItemsVm viewModel);



        List<ContractsItemsVm> FindAll(ContractsItemsSm searchModel);
        bool Delete(int id);
        List<ContractsItemsVm> GetByContractsId(int? id);

    }
}
