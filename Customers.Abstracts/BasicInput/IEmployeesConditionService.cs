﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Customers.Abstracts.BasicInput
{
    public interface IEmployeesConditionService
    {
        EmployeesConditionsVm GetById(int id);

        bool Save(EmployeesConditionsVm viewModel);
        List<EmployeesConditionsVm> FindAll(EmployeesConditionsSm searchModel);
        bool Delete(int id);
        List<EmployeesConditionsVm> GetByEmployeeId(int? id);
    }
}
