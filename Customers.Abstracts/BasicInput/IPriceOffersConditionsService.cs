﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
  public  interface IPriceOffersConditionsService
    {
        PriceOffersConditionVm GetById(int id);
        bool Save(PriceOffersConditionVm viewModel);
        List<PriceOffersConditionVm> FindAll(PriceOffersConditionSm searchModel);
        bool Delete(int id);
        List<PriceOffersConditionVm> GetByPriceOffersId(int? id);
    }
}
