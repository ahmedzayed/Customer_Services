﻿using Customers.Model.Visites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Visites
{
   public interface  IVisitesService
    {
        VisitVm GetById(int id);
        bool Save(VisitVm viewModel);
        List<VisitVm> FindAll(VisitSm searchModel);
        List<VisitVm> GetByCustomerId(int Id);
        bool Delete(int id);
    }
}
