﻿using Customers.Model;
using System.Collections.Generic;


namespace Customers.Abstracts
{
    public interface ILookupService
    {

        #region Marketing
        List<KeyValueLookup> FindEmployees();
        List<KeyValueLookup> FindEngineeringEmp();
        List<KeyValueLookup> FindCustomers();
        List<KeyValueLookup> FindCustomerStatus();
        List<KeyValueLookup> FindOffer();
        List<KeyValueLookup> FindContract();


        #endregion
        #region BasicInput
        List<KeyValueLookup> FindCustomerActivities();
        List<KeyValueLookup> FindServices();

        List<KeyValueLookup> FindItems();
        List<KeyValueLookup> FindConditions();
        List<KeyValueLookup> FindDepartments();
        List<KeyValueLookup> FindCustomerAnalyses();
        List<KeyValueLookup> FindConditionsEmp();


        List<KeyValueLookup> FindWorkSystem();
        List<KeyValueLookup> FindEmployeeType();

        List<KeyValueLookup> FindSpecialization();

        #endregion
    }
}
