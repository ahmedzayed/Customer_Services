﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.SupportRequests
{
    public interface ISupportServicesService
    {
        SupportServicesVm GetById(int id);
        bool Save(SupportServicesVm viewModel);
        List<SupportServicesVm> FindAll(SupportServicesSm searchModel);
        List<SupportServicesVm> GetByCustomerId(int UserId);

        List<SupportServicesVm> GetByEmployeeId(int UserId);
        int Count();

        bool Delete(int id);
    }
}
