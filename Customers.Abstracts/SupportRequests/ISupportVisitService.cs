﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.SupportRequests
{
    public interface ISupportVisitService
    {
        SupportVisitVm GetById(int id);
        bool Save(SupportVisitVm viewModel);
        List<SupportVisitVm> FindAll(SupportVisitSm searchModel);
        List<SupportVisitVm> GetByCustomerId(int UserId);

        List<SupportVisitVm> GetByEmployeeId(int UserId);
        int Count();

        bool Delete(int id);
    }
}
