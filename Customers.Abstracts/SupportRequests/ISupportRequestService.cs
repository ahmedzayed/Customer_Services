﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.SupportRequests
{
    public interface ISupportRequestService
    {
        SupportRequestsVm GetById(int id);
        bool Save(SupportRequestsVm viewModel);
        List<SupportRequestsVm> FindAll(SupportRequestsSm searchModel);
        List<SupportRequestsVm> GetByCustomerId(int UserId);

        List<SupportRequestsVm> GetByEmployeeId(int UserId);

        int Count();

        bool Delete(int id);
    }
}
