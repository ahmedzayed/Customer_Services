﻿using System.Web;

namespace Customers.Helpers
{
   public class Sessions
    {
        public static string CurrentCulture
        {
            get
            {
                return (HttpContext.Current.Session != null && HttpContext.Current.Session["CurrentCulture"] != null) ?
                    HttpContext.Current.Session["CurrentCulture"].ToString() : "ar-EG";
            }
            set { HttpContext.Current.Session["CurrentCulture"] = value; }
        }

        public static string CurrentText
        {
            get
            {
                return (HttpContext.Current.Session != null && HttpContext.Current.Session["CurrentText"] != null) ?
                    HttpContext.Current.Session["CurrentText"].ToString() : "English";
            }
            set
            {
                HttpContext.Current.Session["CurrentText"] = value;
            }
        }
    }
}
