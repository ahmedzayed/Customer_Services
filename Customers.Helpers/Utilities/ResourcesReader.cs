﻿using System.Threading;

namespace Customers.Helpers.Utilities
{
    public class ResourcesReader
    {
        public static string UserNamelogin;
        public static int UserId { get; set; }
        public static int UserTypeId { get; set; }
        public static bool? ISAdmin { get; set; }


        public static bool IsArabic
        {
            get { return Thread.CurrentThread.CurrentUICulture.Name.StartsWith("ar"); }
        }

       
    }
}
