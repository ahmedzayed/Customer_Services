﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
   public partial class AnalysesRequest
    {
        public static AnalysesRequest Clone(AnalysesRequestVm viewmodel)
        {
            return new AnalysesRequest
            {
                AnalysesDate = viewmodel.AnalysesDate,
                Id = viewmodel.Id,
                CreatedDate=viewmodel.CreatedDate,
                CustomerAnalysesId=viewmodel.CustomerAnalysesId,
                CustomerId=viewmodel.CustomerId,
                UpdatedBy=viewmodel.UpdatedBy,
                EngId=viewmodel.EngId,
                UpdatedDate=viewmodel.UpdatedDate,
                CreatedBy = viewmodel.CreatedBy,


            };

        }
    }
}
