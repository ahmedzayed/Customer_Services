﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial  class AnalysesVisits
    {
        public static AnalysesVisits Clone(AnalysesVisitsVm viewmodel)
        {
            return new AnalysesVisits
            {

                Id = viewmodel.Id,

                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                Image = viewmodel.Image,
                
                Nots = viewmodel.Nots,
                AnalysesRequestId = viewmodel.AnalysesRequestId,
                UpdatedBy = viewmodel.UpdatedBy,
                UpdatedDate = viewmodel.UpdatedDate,
                Visitdate = viewmodel.Visitdate,


            };
        }
    }
}
