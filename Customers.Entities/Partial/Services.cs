﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
   public partial class Services
    {
        public static Services Clone(ServicesVm viewmodel)
        {
            return new Services
            {
                ServiceNameAr = viewmodel.ServiceNameAr,
                ServiceNameEn = viewmodel.ServiceNameEn,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                Id = viewmodel.Id,
                Price = viewmodel.Price,
                Time = viewmodel.Time,
                UpdatedBy = viewmodel.UpdatedBy,
                UpdatedDate = viewmodel.UpdatedDate




            };

        }
    }
}
