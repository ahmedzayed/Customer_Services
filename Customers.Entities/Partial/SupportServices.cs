﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
 public partial   class SupportServices
    {
        public static SupportServices Clone(SupportServicesVm viewmodel)
        {
            return new SupportServices
            {
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                CustomerId = viewmodel.CustomerId,
                DepartmentId = viewmodel.DepartmentId,
                Description = viewmodel.Description,
                HourDate = viewmodel.HourDate,
                Id = viewmodel.Id,
                Image = viewmodel.Image,
                ServicesId = viewmodel.ServicesId,



            };
        }
    }
}
