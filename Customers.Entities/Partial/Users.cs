﻿using Customers.Model.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
   public partial class Users
    {
        public static Users Clone(AuthUserVm viewmodel)
        {
            return new Users
            {
                Id = viewmodel.Id,
                IsAdmin = viewmodel.ISAdmin,
                IsActive = viewmodel.IsActive,
                Password = viewmodel.Password,
                TypeId = viewmodel.TypeId,
                UserName = viewmodel.UserName,
                UserId = viewmodel.UserId

            };
        }
    }
}
