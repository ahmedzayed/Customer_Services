﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
    public partial class Items
    {
        public static Items Clone(BasicInputVm viewmodel)
        {
            return new Items
            {
                Id=viewmodel.Id,
                NameAr = viewmodel.NameAr,
                NameEn = viewmodel.NameEn,
                CreatedBy = viewmodel.CreatedBy,
                
                
            };

        }
    }
}
