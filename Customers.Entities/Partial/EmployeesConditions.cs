﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{

    public partial class EmployeesConditions
    {
        public static EmployeesConditions Clone(EmployeesConditionsVm viewModel)
        {
            return new EmployeesConditions
            {
                Id = viewModel.Id,
                ConditionId = viewModel.CondionId,
                EmployeeId = viewModel.EmployeeId,
                CreatedDate = viewModel.CreatedDate,
              
                UpdatedBy = viewModel.UpdatedBy,
                UpdatedDate = viewModel.UpdatedDate,


                CreatedBy = viewModel.CreatedBy,

            };
        }
    }
}