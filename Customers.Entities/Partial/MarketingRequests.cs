﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{

    public partial class MarketingRequests
    {
        public static MarketingRequests Clone(MarketingVm viewmodel)
        {
            return new MarketingRequests
            {
                ActivityId = viewmodel.ActivityId,
                CustId = viewmodel.CustId,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                Id = viewmodel.Id,
                EmployeeId = viewmodel.EmployeeId,
                RequestStatusId = viewmodel.RequestStatusId,
                UpdatedBy = viewmodel.UpdatedBy,
                Note = viewmodel.Note




            };

        }
    }
}

