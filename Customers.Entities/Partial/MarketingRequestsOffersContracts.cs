﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial class MarketingRequestsOffersContracts
    {
        public static MarketingRequestsOffersContracts Clone(RequestOfferContractVm viewmodel)
        {
            return new MarketingRequestsOffersContracts
            {
                Id=viewmodel.Id,
                ContractId = viewmodel.ContractId,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                EndDate=viewmodel.EndDate,
                StartDate=viewmodel.StartDate,
               
                RequestOfferId = viewmodel.RequestOfferId

            };
        }
    }
}
