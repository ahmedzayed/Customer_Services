﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial  class MarketingRequestsOffersContractsConditions
    {
        public static MarketingRequestsOffersContractsConditions Clone(RequestsOffersContractsConditionsVm viewmodel)
        {
            return new MarketingRequestsOffersContractsConditions
            {
                ConditionId = viewmodel.ConditionId,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                Id = viewmodel.Id,
                OfferContractId = viewmodel.OfferContractId

            };

        }
    }
}
