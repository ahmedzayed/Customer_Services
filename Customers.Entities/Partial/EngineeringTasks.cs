﻿using Customers.Model.EngineeringTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
 public partial   class EngineeringTasks
    {
        public static EngineeringTasks Clone(EngineeringTaskVm viewmodel)
        {
            return new EngineeringTasks
            {
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                EmployeeId = viewmodel.EmployeeId,
                EndDate = viewmodel.EndDate,
                Id = viewmodel.Id,
                OrderAddress = viewmodel.OrderAddress,
                OrderCase = viewmodel.OrderCase,
                OrderChangeDate = viewmodel.OrderChangeDate,
                OrderDate = viewmodel.OrderDate,
                OrderDetails = viewmodel.OrderDetails,
                OrderFile = viewmodel.OrderFile,
                OrderNumber = viewmodel.OrderNumber
            };
        }
    }
}
