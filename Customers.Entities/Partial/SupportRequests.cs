﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
 public partial   class SupportRequests
    {
        public static SupportRequests Clone(SupportRequestsVm viewmodel)
        {
            return new SupportRequests
            {
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                CustomerId = viewmodel.CustomerId,
                Date = viewmodel.Date,
                DepartmentId = viewmodel.DepartmentId,
                DescriptionProblem = viewmodel.DescriptionProblem,
                HourDate = viewmodel.HourDate,
                Image = viewmodel.Image,
                Id = viewmodel.Id,
                EmployeeId=viewmodel.EmployeeId,
                Details=viewmodel.Details
            };
        }
    }
}
