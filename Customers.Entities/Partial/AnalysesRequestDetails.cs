﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial  class AnalysesRequestDetails
    {
        public static AnalysesRequestDetails Clone(AnalysesRequestDetailsVm viewmodel)
        {
            return new AnalysesRequestDetails
            {
                CreatdBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                FileName = viewmodel.FileName,
                Id = viewmodel.Id,
                RequestId = viewmodel.RequestId,
                UpdatedBy = viewmodel.UpdatedBy,
                UpdatedDate = viewmodel.UpdatedDate
            };
        }
    }
}
