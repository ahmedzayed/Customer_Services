﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
 public partial  class MarketingRequestsOffersConditions
    {
        public static MarketingRequestsOffersConditions Clone(RequestOfferConditionVm viewmodel)
        {
            return new MarketingRequestsOffersConditions
            {
                ConditionId = viewmodel.ConditionId,
                Id = viewmodel.Id,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                RequestOfferId = viewmodel.RequestOfferId
            };
        }

    }
}
