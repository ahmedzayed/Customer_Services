﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
    partial class CustomerStatus
    {
        public static CustomerStatus Clone(BasicInputVm viewModel)
        {
            return new CustomerStatus
            {
                Id = viewModel.Id,
                NameAr = viewModel.NameAr,
                NameEn = viewModel.NameEn,
                CreatedBy = viewModel.CreatedBy
            };
        }
    }
}
