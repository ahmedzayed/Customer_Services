﻿using Customers.Model.Visites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial  class Visites
    {
        public static Visites Clone(VisitVm viewmodel)
        {
            return new Visites
            {
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                CustomerId = viewmodel.CustomerId,
                EmployeeId = viewmodel.EmployeeId,
                Id = viewmodel.Id,
                ImportantInformation = viewmodel.ImportantInformation,
                IsFix = viewmodel.IsFix,
                VisitDate = DateTime.Now,
                VisitDetails = viewmodel.VisitDetails
            };
        }
    }
}
