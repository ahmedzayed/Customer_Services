﻿using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial  class SupportVisits
    {
        public static SupportVisits Clone(SupportVisitVm viewmodel)
        {
            return new SupportVisits
            {
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                CustomerId = viewmodel.CustomerId,
                Id = viewmodel.Id,
                SuggestedTime = viewmodel.SuggestedTime,
                VisitDate = viewmodel.VisitDate,
                VisitHoure = viewmodel.VisitHoure,
                VisitReason = viewmodel.VisitReason,
                Details=viewmodel.Details,
                EmployeeId=viewmodel.EmployeeId,
                
            };
        }
    }

}
