﻿using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
  public partial   class Working
    {
        public static Working Clone(WorkingVm viewmodel)
        {
            return new Working
            {
                ContractId = viewmodel.ContractId,
                CreatedBy = viewmodel.CreatedBy,
                CreatedDate = viewmodel.CreatedDate,
                CustomerId = viewmodel.CustomerId,
                EndDate = viewmodel.EndDate,
                EndHour = viewmodel.EndHour,
                Id = viewmodel.Id,
                StartDate = viewmodel.StartDate,
                StartHour = viewmodel.StartHour,
                UpdatedBy = viewmodel.UpdatedBy,
                UpdatedDate = viewmodel.UpdatedDate,
               RequestOffersId=viewmodel.RequestOffersId,
            };
        }
    }
}
