﻿using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
   public partial class MarketingRequestsOffers
    {

        public static MarketingRequestsOffers Clone(RequestOfferVm viewmoel)
        {
            return new MarketingRequestsOffers
            {
                Id = viewmoel.Id,
                CreatedBy = viewmoel.CreatedBy,
                CreatedDate = viewmoel.CreatedDate,
                OfferId = viewmoel.OfferId,
                RequestId = viewmoel.RequestId,
                OfferSelected = viewmoel.OfferSelected
            };
        }

    }
}
