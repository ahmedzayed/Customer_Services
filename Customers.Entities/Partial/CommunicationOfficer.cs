﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Entities;

namespace Customers.Entities
{
    public partial class CommunicationOfficer
    {

        public static CommunicationOfficer Clone(CommunicationOfficerVm viewModel)
        {
            return new  CommunicationOfficer
            {
                
                Id = viewModel.Id,
                Name = viewModel.Name,
                PhoneNumber = viewModel.PhoneNumber,
                CreatedDate = viewModel.CreatedDate,
                JobName = viewModel.JobeName,
                Email = viewModel.Email,
                CustomerId = viewModel.CustomerId,
              
                UpdatedBy = viewModel.UpdatedBy,
                UpdatedDate = viewModel.UpdatedDate,


                CreatedBy = viewModel.CreatedBy,

            };
        }
    }
}
