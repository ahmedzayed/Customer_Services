//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Customers.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerStatus
    {
        public CustomerStatus()
        {
            this.MarketingRequests = new HashSet<MarketingRequests>();
            this.MarketingRequestsStatus = new HashSet<MarketingRequestsStatus>();
        }
    
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
    
        public virtual ICollection<MarketingRequests> MarketingRequests { get; set; }
        public virtual ICollection<MarketingRequestsStatus> MarketingRequestsStatus { get; set; }
    }
}
