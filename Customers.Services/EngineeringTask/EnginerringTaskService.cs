﻿using Customers.Abstracts.EngineeringTasks;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.EngineeringTask;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.EngineeringTask
{
   public class EnginerringTaskService : BaseService, IEngineeringTasksService
    {
        public EngineeringTaskVm GetById(int id)
        {
            IQueryable<EngineeringTaskVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.EngineeringTasks.Where(m => m.Id == id).Select(model =>
                new EngineeringTaskVm
                {
                    Id = model.Id,
                    OrderAddress = model.OrderAddress,
                    OrderCase = model.OrderCase,
                    OrderChangeDate = model.OrderChangeDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    CreatedDate=model.CreatedDate,
                    EmployeeId=model.EmployeeId ??0,
                    EndDate=model.EndDate,
                    OrderDate=model.OrderDate,
                    OrderDetails=model.OrderDetails,
                    OrderFile=model.OrderFile,
                    OrderNumber=model.OrderNumber,
                    

                });
            }
            else
            {
                Details = Context.EngineeringTasks.Where(m => m.Id == id).Select(model =>
               new EngineeringTaskVm
               {
                   Id = model.Id,
                   OrderAddress = model.OrderAddress,
                   OrderCase = model.OrderCase,
                   OrderChangeDate = model.OrderChangeDate,
                   CreatedBy = model.CreatedBy ?? 0,
                   CreatedDate = model.CreatedDate,
                   EmployeeId = model.EmployeeId ?? 0,
                   EndDate = model.EndDate,
                   OrderDate = model.OrderDate,
                   OrderDetails = model.OrderDetails,
                   OrderFile = model.OrderFile,
                   OrderNumber = model.OrderNumber,
               });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(EngineeringTaskVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(EngineeringTasks.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.EngineeringTasks.Add(EngineeringTasks.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<EngineeringTaskVm> FindAll(EngineeringTaskSm searchModel)
        {
            IQueryable<EngineeringTaskVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from model in Context.EngineeringTasks
                          select new EngineeringTaskVm
                          {
               
                   Id = model.Id,
                   OrderAddress = model.OrderAddress,
                   OrderCase = model.OrderCase,
                   OrderChangeDate = model.OrderChangeDate,
                   CreatedBy = model.CreatedBy ?? 0,
                   CreatedDate = model.CreatedDate,
                   EmployeeId = model.EmployeeId ?? 0,
                   EndDate = model.EndDate,
                   OrderDate = model.OrderDate,
                   OrderDetails = model.OrderDetails,
                   OrderFile = model.OrderFile,
                   OrderNumber = model.OrderNumber,
               };
            }
            else
            {
                Details = from model in Context.EngineeringTasks
                          select new EngineeringTaskVm
                          {

                              Id = model.Id,
                              OrderAddress = model.OrderAddress,
                              OrderCase = model.OrderCase,
                              OrderChangeDate = model.OrderChangeDate,
                              CreatedBy = model.CreatedBy ?? 0,
                              CreatedDate = model.CreatedDate,
                              EmployeeId = model.EmployeeId ?? 0,
                              EndDate = model.EndDate,
                              OrderDate = model.OrderDate,
                              OrderDetails = model.OrderDetails,
                              OrderFile = model.OrderFile,
                              OrderNumber = model.OrderNumber,
                          };
            }
            return Details.ToList();
        }

        public List<EngineeringTaskVm> GetByEmployeeId(int id)
        {
            IQueryable<EngineeringTaskVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.EngineeringTasks.Where(m => m.EmployeeId == id).Select(model =>
                new EngineeringTaskVm
                {
                    Id = model.Id,
                    OrderAddress = model.OrderAddress,
                    OrderCase = model.OrderCase,
                    OrderChangeDate = model.OrderChangeDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    CreatedDate = model.CreatedDate,
                    EmployeeId = model.EmployeeId ?? 0,
                    EndDate = model.EndDate,
                    OrderDate = model.OrderDate,
                    OrderDetails = model.OrderDetails,
                    OrderFile = model.OrderFile,
                    OrderNumber = model.OrderNumber,
                });
            }
            else
            {
                Details = Context.EngineeringTasks.Where(m => m.EmployeeId == id).Select(model =>
                 new EngineeringTaskVm
                 {
                     Id = model.Id,
                     OrderAddress = model.OrderAddress,
                     OrderCase = model.OrderCase,
                     OrderChangeDate = model.OrderChangeDate,
                     CreatedBy = model.CreatedBy ?? 0,
                     CreatedDate = model.CreatedDate,
                     EmployeeId = model.EmployeeId ?? 0,
                     EndDate = model.EndDate,
                     OrderDate = model.OrderDate,
                     OrderDetails = model.OrderDetails,
                     OrderFile = model.OrderFile,
                     OrderNumber = model.OrderNumber,
                 });
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new EngineeringTasks { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }


    }
}
