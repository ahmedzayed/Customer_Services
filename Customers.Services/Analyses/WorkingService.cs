﻿using Customers.Abstracts.Analyses;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Analyses
{
  public  class WorkingService : BaseService, IWorkingService
    {
        public int Count()
        {
            return Context.Working.Count();
        }
        
        public WorkingVm GetById(int id)
        {
            IQueryable<WorkingVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Working.Where(m => m.Id == id).Select(model =>
                new WorkingVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    CustomerId = model.CustomerId ??0,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    EndHour=model.EndHour,
                    StartHour=model.StartHour,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Working.Where(m => m.Id == id).Select(model =>
                new WorkingVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    CustomerId = model.CustomerId ?? 0,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    EndHour = model.EndHour,
                    StartHour = model.StartHour,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }



        public List <WorkingVm> GetWorkingList(int id, int RequestId)
        {
            IQueryable<WorkingVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Working.Where(m => m.CustomerId == id &&m.RequestOffersId==RequestId).Select(model =>
                new WorkingVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    CustomerId = model.CustomerId ?? 0,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    EndHour = model.EndHour,
                    StartHour = model.StartHour,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Working.Where(m => m.CustomerId == id && m.RequestOffersId==RequestId).Select(model =>
                new WorkingVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    CustomerId = model.CustomerId ?? 0,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    EndHour = model.EndHour,
                    StartHour = model.StartHour,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.ToList();
        }
        public bool Save(WorkingVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(Working.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.Working.Add(Working.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public List<WorkingVm> FindAll(WorkingSm searchModel)
        {
            IQueryable<WorkingVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from model in Context.Working
                          select new WorkingVm
                          {
                              Id = model.Id,
                              ContractId = model.ContractId ?? 0,
                              CustomerId = model.CustomerId ?? 0,
                              EndDate = model.EndDate,
                              StartDate = model.StartDate,
                              EndHour = model.EndHour,
                              StartHour = model.StartHour,

                              CreatedDate = model.CreatedDate,

                              UpdatedBy = model.UpdatedBy ?? 0,
                              UpdatedDate = model.UpdatedDate,
                              CreatedBy = model.CreatedBy ?? 0

                          };
            }
            else
            {
                Details = from model in Context.Working
                          select new WorkingVm
                          {
                              Id = model.Id,
                              ContractId = model.ContractId ?? 0,
                              CustomerId = model.CustomerId ?? 0,
                              EndDate = model.EndDate,
                              StartDate = model.StartDate,
                              EndHour = model.EndHour,
                              StartHour = model.StartHour,

                              CreatedDate = model.CreatedDate,

                              UpdatedBy = model.UpdatedBy ?? 0,
                              UpdatedDate = model.UpdatedDate,
                              CreatedBy = model.CreatedBy ?? 0

                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new Working { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}