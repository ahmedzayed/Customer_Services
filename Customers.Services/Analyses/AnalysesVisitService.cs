﻿using Customers.Abstracts.Analyses;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Analyses
{
   public class AnalysesVisitService : BaseService, IAnalysesVisitService
    {
        public int Count()
        {
            return Context.AnalysesRequest.Count();
        }
        public AnalysesVisitsVm GetById(int id)
        {
            IQueryable<AnalysesVisitsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesVisits.Where(m => m.Id == id).Select(model =>
                new AnalysesVisitsVm
                {
                    Id = model.Id,
                 
                    Visitdate = model.Visitdate ,
                    AnalysesRequestId=model.AnalysesRequestId??0,
                    Nots=model.Nots,



                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.AnalysesVisits.Where(m => m.Id == id).Select(model =>
                   new AnalysesVisitsVm
                   {
                       Id = model.Id,

                       Visitdate = model.Visitdate,
                       AnalysesRequestId = model.AnalysesRequestId ?? 0,
                       Nots = model.Nots,



                       CreatedDate = model.CreatedDate,

                       UpdatedBy = model.UpdatedBy ?? 0,
                       UpdatedDate = model.UpdatedDate,
                       CreatedBy = model.CreatedBy ?? 0
                   });
            }
            return Details.FirstOrDefault();
        }

      

        public bool Save(AnalysesVisitsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(AnalysesVisits.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.AnalysesVisits.Add(AnalysesVisits.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<AnalysesVisitsVm> GetByRequestId(int id)
        {
            IQueryable<AnalysesVisitsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesVisits.Where(m => m.AnalysesRequestId == id).Select(model =>
                new AnalysesVisitsVm
                {
                    Id = model.Id,
                    AnalysesRequestId = model.AnalysesRequestId ?? 0,
                    Nots = model.Nots,
                    Image = model.Image,
                    Visitdate = model.Visitdate,



                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,

                });
            }
            else
            {
                Details = Context.AnalysesVisits.Where(m => m.AnalysesRequestId == id).Select(model =>
                  new AnalysesVisitsVm
                  {
                      Id = model.Id,
                      AnalysesRequestId = model.AnalysesRequestId ?? 0,
                      Nots = model.Nots,
                      Image = model.Image,
                      Visitdate = model.Visitdate,



                      CreatedDate = model.CreatedDate,

                      UpdatedBy = model.UpdatedBy ?? 0,
                      UpdatedDate = model.UpdatedDate,
                  });
            }
            return Details.ToList();
        }

        public List<AnalysesVisitsVm> FindAll(AnalysesVisitsSm searchModel)
        {
            IQueryable<AnalysesVisitsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.AnalysesVisits
                          select new AnalysesVisitsVm
                          {
                              Id = data.Id,

                              Visitdate = data.Visitdate,
                              AnalysesRequestId = data.AnalysesRequestId ?? 0,
                              Nots = data.Nots,



                              CreatedDate = data.CreatedDate,

                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.AnalysesVisits
                          select new AnalysesVisitsVm
                          {
                              Id = data.Id,

                              Visitdate = data.Visitdate,
                              AnalysesRequestId = data.AnalysesRequestId ?? 0,
                              Nots = data.Nots,
                             



                              CreatedDate = data.CreatedDate,

                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new AnalysesVisits { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}