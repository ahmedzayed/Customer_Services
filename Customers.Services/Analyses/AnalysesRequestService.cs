﻿using Customers.Abstracts.Analyses;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Analyses
{
  public  class AnalysesRequestService : BaseService, IAnalysesRequestService
    {
        public int Count()
        {
            return Context.AnalysesRequest.Count();
        }
        public AnalysesRequestVm GetById(int id)
        {
            IQueryable<AnalysesRequestVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesRequest.Where(m => m.Id == id).Select(model =>
                new AnalysesRequestVm
                {
                    Id = model.Id,
                    CustomerId = model.CustomerId ?? 0,
                    CustName = model.Customers.NameAr,
                    AnalysesDate = model.AnalysesDate ,
                 CustomerAnalysesId=model.CustomerAnalysesId ?? 0,
                    EngId = model.EngId ?? 0,
                    EngName = model.Employees.NameAr,


                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ??0,
                    UpdatedDate = model.UpdatedDate,
                    CustomerAnalysesName=model.CustomerAnalyses.NameAr,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.AnalysesRequest.Where(m => m.Id == id).Select(model =>
                new AnalysesRequestVm
                {
                    Id = model.Id,
                    CustomerId = model.CustomerId ?? 0,
                    CustName = model.Customers.NameAr,
                    AnalysesDate = model.AnalysesDate,
                    CustomerAnalysesId = model.CustomerAnalysesId ?? 0,
                    CustomerAnalysesName=model.CustomerAnalyses.NameAr,

                    EngId = model.EngId ?? 0,
                    EngName = model.Employees.NameAr,
                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public List<AnalysesRequestVm> GetByCustomerId(int id)
        {
            IQueryable<AnalysesRequestVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesRequest.Where(m => m.CustomerId == id).Select(model =>
                new AnalysesRequestVm
                {
                    Id = model.Id,
                    CustomerId = model.CustomerId ?? 0,
                    CustName = model.Customers.NameAr,
                    AnalysesDate = model.AnalysesDate,
                    EngId=model.EngId ??0,
                    EngName=model.Employees.NameAr,
                    CustomerAnalysesId = model.CustomerAnalysesId ?? 0,



                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CustomerAnalysesName = model.CustomerAnalyses.NameAr,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.AnalysesRequest.Where(m => m.CustomerId == id).Select(model =>
                new AnalysesRequestVm
                {
                    Id = model.Id,
                    CustomerId = model.CustomerId ?? 0,
                    EngId = model.EngId ?? 0,
                    EngName = model.Employees.NameAr,
                    CustName = model.Customers.NameAr,
                    AnalysesDate = model.AnalysesDate,
                    CustomerAnalysesId = model.CustomerAnalysesId ?? 0,
                    CustomerAnalysesName = model.CustomerAnalyses.NameAr,


                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.ToList();
        }

        public bool Save(AnalysesRequestVm viewModel)
        {
            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(AnalysesRequest.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.AnalysesRequest.Add(AnalysesRequest.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public List<AnalysesRequestVm> FindAll(AnalysesRequestSm searchModel)
        {
            IQueryable<AnalysesRequestVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.AnalysesRequest
                          select new AnalysesRequestVm
                          {
                              Id = data.Id,
                              CustomerId = data.CustomerId ?? 0,
                              CustName = data.Customers.NameAr,
                              CreatedDate = data.CreatedDate,
                              CustomerAnalysesId = data.CustomerAnalysesId ??0,
                              //Time = data.Time,
                              EngId=data.EngId ?? 0,
                              EngName=data.Employees.NameAr,
                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0,
                              CustomerAnalysesName = data.CustomerAnalyses.NameAr ,
                             AnalysesDate=data.AnalysesDate ,
                          };
            }
            else
            {
                Details = from data in Context.AnalysesRequest
                          select new AnalysesRequestVm
                          {
                              Id = data.Id,
                              CustomerId = data.CustomerId ?? 0,
                              CustName = data.Customers.NameAr,
                              CreatedDate = data.CreatedDate,
                              CustomerAnalysesId = data.CustomerAnalysesId ?? 0,
                              //Time = data.Time,
                              EngId = data.EngId ?? 0,
                              EngName = data.Employees.NameAr,
                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0,
                              CustomerAnalysesName = data.CustomerAnalyses.NameAr,
                              AnalysesDate = data.AnalysesDate,
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new AnalysesRequest { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}