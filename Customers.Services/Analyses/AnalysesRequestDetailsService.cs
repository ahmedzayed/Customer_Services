﻿using Customers.Abstracts.Analyses;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Analyses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Analyses
{
   public class AnalysesRequestDetailsService : BaseService, IAnalysesRequestDetailsService
    {
        public int Count()
        {
            return Context.AnalysesRequest.Count();
        }
        public List<AnalysesRequestDetailsVm> GetByRequestId(int id)
        {
            IQueryable<AnalysesRequestDetailsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesRequestDetails.Where(m => m.RequestId == id).Select(model =>
                new AnalysesRequestDetailsVm
                {
                    Id = model.Id,
                    RequestId = model.RequestId ?? 0,
                    CustName = model.AnalysesRequest.Customers.NameAr,
                    AnalysesName = model.AnalysesRequest.CustomerAnalyses.NameAr,
                    FileName = model.FileName ,



                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                 
                });
            }
            else
            {
                Details = Context.AnalysesRequestDetails.Where(m => m.RequestId == id).Select(model =>
                  new AnalysesRequestDetailsVm
                  {
                      Id = model.Id,
                      RequestId = model.RequestId ?? 0,
                      CustName = model.AnalysesRequest.Customers.NameAr,
                      AnalysesName = model.AnalysesRequest.CustomerAnalyses.NameAr,
                      FileName = model.FileName,



                      CreatedDate = model.CreatedDate,

                      UpdatedBy = model.UpdatedBy ?? 0,
                      UpdatedDate = model.UpdatedDate,
                  });
            }
            return Details.ToList();
        }
        public AnalysesRequestDetailsVm GetById(int id)
        {
            IQueryable<AnalysesRequestDetailsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.AnalysesRequestDetails.Where(m => m.Id == id).Select(model =>
                new AnalysesRequestDetailsVm
                {
                    Id = model.Id,
                    RequestId = model.RequestId ?? 0,
                    FileName = model.FileName,
                    AnalysesName=model.AnalysesRequest.CustomerAnalyses.NameAr,
                    CustName=model.AnalysesRequest.Customers.NameAr,
                    

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatdBy ??0
                });
            }
            else
            {
                Details = Context.AnalysesRequestDetails.Where(m => m.Id == id).Select(model =>
                new AnalysesRequestDetailsVm
                {
                    Id = model.Id,
                    RequestId = model.RequestId ?? 0,
                    FileName = model.FileName,
                    AnalysesName = model.AnalysesRequest.CustomerAnalyses.NameAr,
                    CustName = model.AnalysesRequest.Customers.NameAr,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy ?? 0,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatdBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

       

        public bool Save(AnalysesRequestDetailsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(AnalysesRequestDetails.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.AnalysesRequestDetails.Add(AnalysesRequestDetails.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public List<AnalysesRequestDetailsVm> FindAll(AnalysesRequestDetailsSm searchModel)
        {
            IQueryable<AnalysesRequestDetailsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.AnalysesRequestDetails
                          select new AnalysesRequestDetailsVm
                          {
                              Id = data.Id,
                              RequestId = data.RequestId ?? 0,
                              FileName = data.FileName,
                              CreatedDate = data.CreatedDate,
                            CustName=data.AnalysesRequest.Customers.NameAr,
                            AnalysesName=data.AnalysesRequest.CustomerAnalyses.NameAr,
                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatdBy ?? 0,
                             
                          };
            }
            else
            {
                Details = from data in Context.AnalysesRequestDetails
                          select new AnalysesRequestDetailsVm
                          {
                              Id = data.Id,
                              RequestId = data.RequestId ?? 0,
                              FileName = data.FileName,
                              CreatedDate = data.CreatedDate,
                              CustName = data.AnalysesRequest.Customers.NameAr,
                              AnalysesName = data.AnalysesRequest.CustomerAnalyses.NameAr,
                              UpdatedBy = data.UpdatedBy ?? 0,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatdBy ?? 0,

                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new AnalysesRequestDetails { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}