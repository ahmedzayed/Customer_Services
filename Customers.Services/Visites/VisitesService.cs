﻿using Customers.Abstracts.Visites;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Visites;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Visites
{
  public  class VisitesService : BaseService, IVisitesService
    {
        public VisitVm GetById(int id)
        {
            IQueryable<VisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Visites.Where(m => m.Id == id).Select(model =>
                new VisitVm
                {
                    Id = model.Id,
                    VisitDetails = model.VisitDetails,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ??0,
                    CustomerName=model.Customers.NameAr,
                    EmployeeId=model.EmployeeId??0,
                    EmployeeName=model.Employees.NameAr,
                    ImportantInformation=model.ImportantInformation,
                    IsFix=model.IsFix,
                    VisitDate=model.VisitDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Visites.Where(m => m.Id == id).Select(model =>
                 new VisitVm
                 {
                     Id = model.Id,
                     VisitDetails = model.VisitDetails,
                     CreatedDate = model.CreatedDate,
                     CustomerId = model.CustomerId ?? 0,
                     CustomerName = model.Customers.NameAr,
                     EmployeeId = model.EmployeeId ?? 0,
                     EmployeeName = model.Employees.NameAr,
                     ImportantInformation = model.ImportantInformation,
                     IsFix = model.IsFix,
                     VisitDate = model.VisitDate,
                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(VisitVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(Customers.Entities.Visites.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.Visites.Add(Customers.Entities.Visites.Clone(viewModel));
            }
            return ContextSaveChanges();
        }
        public List<VisitVm> GetByCustomerId(int id)
        {
            IQueryable<VisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Visites.Where(m => m.CustomerId == id).Select(model =>
                new VisitVm
                {
                    Id = model.Id,
                    VisitDetails = model.VisitDetails,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    CustomerName = model.Customers.NameAr,
                    EmployeeId = model.EmployeeId ?? 0,
                    EmployeeName = model.Employees.NameAr,
                    ImportantInformation = model.ImportantInformation,
                    IsFix = model.IsFix,
                    VisitDate = model.VisitDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Visites.Where(m => m.CustomerId == id).Select(model =>
                new VisitVm
                {
                    Id = model.Id,
                    VisitDetails = model.VisitDetails,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    CustomerName = model.Customers.NameAr,
                    EmployeeId = model.EmployeeId ?? 0,
                    EmployeeName = model.Employees.NameAr,
                    ImportantInformation = model.ImportantInformation,
                    IsFix = model.IsFix,
                    VisitDate = model.VisitDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.ToList();
        }
        public List<VisitVm> FindAll(VisitSm searchModel)
        {
            IQueryable<VisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from model in Context.Visites
                          select new VisitVm
                          {
                              Id = model.Id,
                              VisitDetails = model.VisitDetails,
                              CreatedDate = model.CreatedDate,
                              CustomerId = model.CustomerId ?? 0,
                              CustomerName = model.Customers.NameAr,
                              EmployeeId = model.EmployeeId ?? 0,
                              EmployeeName = model.Employees.NameAr,
                              ImportantInformation = model.ImportantInformation,
                              IsFix = model.IsFix,
                              VisitDate = model.VisitDate,
                              CreatedBy = model.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from model in Context.Visites
                          select new VisitVm
                          {
                              Id = model.Id,
                              VisitDetails = model.VisitDetails,
                              CreatedDate = model.CreatedDate,
                              CustomerId = model.CustomerId ?? 0,
                              CustomerName = model.Customers.NameAr,
                              EmployeeId = model.EmployeeId ?? 0,
                              EmployeeName = model.Employees.NameAr,
                              ImportantInformation = model.ImportantInformation,
                              IsFix = model.IsFix,
                              VisitDate = model.VisitDate,
                              CreatedBy = model.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.Visites { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }
    }
}