﻿using Customers.Abstracts.SupportRequests;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.SupportVisit
{
    public class SupportVisitService : BaseService, ISupportVisitService
    {
        public SupportVisitVm GetById(int id)
        {
            IQueryable<SupportVisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportVisits.Where(m => m.Id == id).Select(model =>
                new SupportVisitVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    SuggestedTime = model.SuggestedTime,
                    CreatedBy = model.CreatedBy ?? 0,
                    VisitDate = model.VisitDate,
                    VisitHoure = model.VisitHoure,
                    VisitReason = model.VisitReason,
                    CustomerName=model.Customers.NameAr,
                    Details=model.Details,
                    EmployeeName=model.Employees.NameAr,

                });
            }
            else
            {
                Details = Context.SupportVisits.Where(m => m.Id == id).Select(model =>
                new SupportVisitVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    SuggestedTime = model.SuggestedTime,
                    CreatedBy = model.CreatedBy ?? 0,
                    VisitDate = model.VisitDate,
                    VisitHoure = model.VisitHoure,
                    VisitReason = model.VisitReason,
                    CustomerName = model.Customers.NameEn,
                    Details = model.Details,
                    EmployeeName = model.Employees.NameEn,
                    

                });
            }
            return Details.FirstOrDefault();
        }
        public int Count()
        {
            return Context.SupportVisits.Count();
        }
        public bool Save(SupportVisitVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(SupportVisits.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.SupportVisits.Add(SupportVisits.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<SupportVisitVm> FindAll(SupportVisitSm searchModel)
        {
            IQueryable<SupportVisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.SupportVisits
                          select new SupportVisitVm
                          {

                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              SuggestedTime = data.SuggestedTime,
                              CreatedBy = data.CreatedBy ?? 0,
                              VisitDate = data.VisitDate,
                              VisitHoure = data.VisitHoure,
                              VisitReason = data.VisitReason,
                              CustomerName = data.Customers.NameAr,
                              Details = data.Details,
                              EmployeeName = data.Employees.NameAr,
                              EmployeeId=data.EmployeeId ??0
                          };
            }
            else
            {
                Details = from data in Context.SupportVisits
                          select new SupportVisitVm
                          {
                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              SuggestedTime = data.SuggestedTime,
                              CreatedBy = data.CreatedBy ?? 0,
                              VisitDate = data.VisitDate,
                              VisitHoure = data.VisitHoure,
                              VisitReason = data.VisitReason,
                              CustomerName = data.Customers.NameEn,
                              Details = data.Details,
                              EmployeeName = data.Employees.NameEn,
                              EmployeeId=data.EmployeeId??0
                          };
            }
            return Details.ToList();
        }
        public List<SupportVisitVm> GetByCustomerId(int UserId)
        {
            IQueryable<SupportVisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportVisits.Where(m => m.CustomerId == UserId).Select(model =>
                new SupportVisitVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    VisitDate = model.VisitDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,
                    SuggestedTime = model.SuggestedTime,
                     
                    CustomerName = model.Customers.NameAr,
                    EmployeeName = model.Employees.NameAr,
                    Details = model.Details,
                    VisitHoure=model.VisitHoure,
                    VisitReason=model.VisitReason

                });
            }
            else
            {
                Details = Context.SupportVisits.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportVisitVm
                  {
                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      VisitDate = model.VisitDate,
                      CreatedBy = model.CreatedBy ?? 0,
                      EmployeeId = model.EmployeeId ?? 0,
                      SuggestedTime = model.SuggestedTime,

                      CustomerName = model.Customers.NameAr,
                      EmployeeName = model.Employees.NameAr,
                      Details = model.Details,
                      VisitHoure = model.VisitHoure,
                      VisitReason = model.VisitReason

                  });
            }
            return Details.ToList();
        }

        public List<SupportVisitVm> GetByEmployeeId(int UserId)
        {
            IQueryable<SupportVisitVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportVisits.Where(m => m.EmployeeId == UserId).Select(model =>
                new SupportVisitVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    VisitDate = model.VisitDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,
                    SuggestedTime = model.SuggestedTime,

                    CustomerName = model.Customers.NameAr,
                    EmployeeName = model.Employees.NameAr,
                    Details = model.Details,
                    VisitHoure = model.VisitHoure,
                    VisitReason = model.VisitReason


                });
            }
            else
            {
                Details = Context.SupportVisits.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportVisitVm
                  {
                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      VisitDate = model.VisitDate,
                      CreatedBy = model.CreatedBy ?? 0,
                      EmployeeId = model.EmployeeId ?? 0,
                      SuggestedTime = model.SuggestedTime,

                      CustomerName = model.Customers.NameAr,
                      EmployeeName = model.Employees.NameAr,
                      Details = model.Details,
                      VisitHoure = model.VisitHoure,
                      VisitReason = model.VisitReason

                  });
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new SupportVisits { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

       
    }
}