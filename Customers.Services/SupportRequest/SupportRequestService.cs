﻿using Customers.Abstracts.SupportRequests;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.SupportRequest
{
   public class SupportRequestService : BaseService, ISupportRequestService
    {
        public int Count()
        {
            return Context.SupportRequests.Count();
        }
        public SupportRequestsVm GetById(int id)
        {
            IQueryable<SupportRequestsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportRequests.Where(m => m.Id == id).Select(model =>
                new SupportRequestsVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ??0,
                    Date = model.Date,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentId=model.DepartmentId ??0,
                    DescriptionProblem=model.DescriptionProblem,
                    HourDate=model.HourDate,
                    Image=model.Image,
                    CustomerName=model.Customers.NameAr,
                    EmployeeName=model.Employees.NameAr,
                    Details=model.Details,
                    DepartmentName=model.Departments.DepartmentNameAr
                    
                });
            }
            else
            {
                Details = Context.SupportRequests.Where(m => m.Id == id).Select(model =>
                new SupportRequestsVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    Date = model.Date,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentId = model.DepartmentId ?? 0,
                    DescriptionProblem = model.DescriptionProblem,
                    HourDate = model.HourDate,
                    Image = model.Image,
                    CustomerName=model.Customers.NameEn,
                    EmployeeName=model.Employees.NameEn,
                    Details=model.Details,
                    DepartmentName = model.Departments.DepartmentNameEn



                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(SupportRequestsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(SupportRequests.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.SupportRequests.Add(SupportRequests.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<SupportRequestsVm> FindAll(SupportRequestsSm searchModel)
        {
            IQueryable<SupportRequestsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.SupportRequests
                          select new SupportRequestsVm
                          {

                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              Date = data.Date,
                              CreatedBy = data.CreatedBy ?? 0,
                              DepartmentId = data.DepartmentId ?? 0,
                              DescriptionProblem = data.DescriptionProblem,
                              HourDate = data.HourDate,
                              Image = data.Image,
                              CustomerName=data.Customers.NameAr,
                              Details=data.Details,
                              EmployeeName=data.Employees.NameAr,
                              DepartmentName = data.Departments.DepartmentNameAr

                          };
            }
            else
            {
                Details = from data in Context.SupportRequests
                          select new SupportRequestsVm
                          {
                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              Date = data.Date,
                              CreatedBy = data.CreatedBy ?? 0,
                              DepartmentId = data.DepartmentId ?? 0,
                              DescriptionProblem = data.DescriptionProblem,
                              HourDate = data.HourDate,
                              DepartmentName = data.Departments.DepartmentNameEn,

                              Image = data.Image,
                              EmployeeName = data.Employees.NameEn,
                              CustomerName = data.Customers.NameEn,
                              Details = data.Details
                          };
            }
            return Details.ToList();
        }

        public List<SupportRequestsVm> GetByCustomerId(int UserId)
        {
            IQueryable<SupportRequestsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportRequests.Where(m => m.CustomerId == UserId).Select(model =>
                new SupportRequestsVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    Date = model.Date,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentId = model.DepartmentId ?? 0,
                    DescriptionProblem = model.DescriptionProblem,
                    HourDate = model.HourDate,
                    Image = model.Image,
                    CustomerName=model.Customers.NameAr,
                    DepartmentName=model.Departments.DepartmentNameAr,
                    EmployeeName=model.Employees.NameAr,
                    Details=model.Details,

                });
            }
            else
            {
                Details = Context.SupportRequests.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportRequestsVm
                  {
                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      Date = model.Date,
                      CreatedBy = model.CreatedBy ?? 0,
                      DepartmentId = model.DepartmentId ?? 0,
                      DescriptionProblem = model.DescriptionProblem,
                      HourDate = model.HourDate,
                      Image = model.Image,
                      CustomerName = model.Customers.NameEn,
                      DepartmentName = model.Departments.DepartmentNameEn,
                      EmployeeName = model.Employees.NameEn,
                      Details = model.Details,
                  });
            }
            return Details.ToList();
        }

         public List<SupportRequestsVm> GetByEmployeeId(int UserId)
        {
            IQueryable<SupportRequestsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportRequests.Where(m => m.EmployeeId == UserId).Select(model =>
                new SupportRequestsVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    Date = model.Date,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentId = model.DepartmentId ?? 0,
                    DescriptionProblem = model.DescriptionProblem,
                    HourDate = model.HourDate,
                    Image = model.Image,
                    CustomerName = model.Customers.NameAr,
                    DepartmentName = model.Departments.DepartmentNameAr,
                    EmployeeName = model.Employees.NameAr,
                    Details = model.Details,

                });
            }
            else
            {
                Details = Context.SupportRequests.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportRequestsVm
                  {
                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      Date = model.Date,
                      CreatedBy = model.CreatedBy ?? 0,
                      DepartmentId = model.DepartmentId ?? 0,
                      DescriptionProblem = model.DescriptionProblem,
                      HourDate = model.HourDate,
                      Image = model.Image,
                      CustomerName = model.Customers.NameEn,
                      DepartmentName = model.Departments.DepartmentNameEn,
                      EmployeeName = model.Employees.NameEn,
                      Details = model.Details,
                  });
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new SupportRequests { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }
    }
}