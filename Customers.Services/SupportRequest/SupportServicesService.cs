﻿using Customers.Abstracts.SupportRequests;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.SupportRequests;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.SupportRequest
{
   public class SupportServicesService : BaseService, ISupportServicesService
    {
        public SupportServicesVm GetById(int id)
        {
            IQueryable<SupportServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportServices.Where(m => m.Id == id).Select(model =>
                new SupportServicesVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                   DepartmentName=model.Departments.DepartmentNameAr,
                     ServicesName=model.Services.ServiceNameAr,
                    CustomerId = model.CustomerId ?? 0,
                    HourDate = model.HourDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentId = model.DepartmentId ??0,
                    Description = model.Description,
                    Image = model.Image,
                    ServicesId=model.ServicesId ??0

                });
            }
            else
            {
                Details = Context.SupportServices.Where(m => m.Id == id).Select(model =>
                new SupportServicesVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    HourDate = model.HourDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    DepartmentName = model.Departments.DepartmentNameEn,
                    ServicesName = model.Services.ServiceNameEn,
                    DepartmentId = model.DepartmentId ?? 0,
                    Description = model.Description,
                    Image = model.Image,
                    ServicesId = model.ServicesId ?? 0

                });
            }
            return Details.FirstOrDefault();
        }

        public int Count()
        {
            return Context.SupportServices.Count();
        }
        public bool Save(SupportServicesVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(SupportServices.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.SupportServices.Add(SupportServices.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<SupportServicesVm> FindAll(SupportServicesSm searchModel)
        {
            IQueryable<SupportServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.SupportServices
                          select new SupportServicesVm
                          {

                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              HourDate = data.HourDate,
                              DepartmentName = data.Departments.DepartmentNameAr,
                              ServicesName = data.Services.ServiceNameAr,
                              CreatedBy = data.CreatedBy ?? 0,
                              DepartmentId = data.DepartmentId ?? 0,
                              Description = data.Description,
                              Image = data.Image,
                              ServicesId = data.ServicesId ?? 0,
                              Details=data.Details,
                              CustomerName=data.Customers.NameAr,
                              EmployeeId=data.EmployeeId ??0,
                              EmployeeName=data.Employees.NameAr,
                          };
            }
            else
            {
                Details = from data in Context.SupportServices
                          select new SupportServicesVm
                          {
                              Id = data.Id,
                              CreatedDate = data.CreatedDate,
                              CustomerId = data.CustomerId ?? 0,
                              HourDate = data.HourDate,
                              DepartmentName = data.Departments.DepartmentNameEn,
                              ServicesName = data.Services.ServiceNameEn,
                              CreatedBy = data.CreatedBy ?? 0,
                              DepartmentId = data.DepartmentId ?? 0,
                              Description = data.Description,
                              Image = data.Image,
                              ServicesId = data.ServicesId ?? 0,
                              Details = data.Details,
                              CustomerName = data.Customers.NameEn,
                              EmployeeId = data.EmployeeId ??0,
                              EmployeeName = data.Employees.NameEn,
                          };
            }
            return Details.ToList();
        }

        public List<SupportServicesVm> GetByCustomerId(int UserId)
        {
            IQueryable<SupportServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportServices.Where(m => m.CustomerId == UserId).Select(model =>
                new SupportServicesVm
                {
                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    HourDate = model.HourDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,

                    CustomerName = model.Customers.NameAr,
                    EmployeeName = model.Employees.NameAr,
                    Details = model.Details,
                   DepartmentId=model.DepartmentId ??0,
                   DepartmentName=model.Departments.DepartmentNameAr,
                   Description=model.Description,
                   Image=model.Image,
                   ServicesId=model.ServicesId ??0,
                   ServicesName=model.Services.ServiceNameAr
                   

                });
            }
            else
            {
                Details = Context.SupportServices.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportServicesVm
                  {
                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      HourDate = model.HourDate,
                      CreatedBy = model.CreatedBy ?? 0,
                      EmployeeId = model.EmployeeId ?? 0,

                      CustomerName = model.Customers.NameAr,
                      EmployeeName = model.Employees.NameAr,
                      Details = model.Details,
                      DepartmentId = model.DepartmentId ?? 0,
                      DepartmentName = model.Departments.DepartmentNameAr,
                      Description = model.Description,
                      Image = model.Image,
                      ServicesId = model.ServicesId ?? 0,
                      ServicesName = model.Services.ServiceNameAr

                  });
            }
            return Details.ToList();
        }

        public List<SupportServicesVm> GetByEmployeeId(int UserId)
        {
            IQueryable<SupportServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.SupportServices.Where(m => m.EmployeeId == UserId).Select(model =>
                new SupportServicesVm
                {

                    Id = model.Id,
                    CreatedDate = model.CreatedDate,
                    CustomerId = model.CustomerId ?? 0,
                    HourDate = model.HourDate,
                    CreatedBy = model.CreatedBy ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,

                    CustomerName = model.Customers.NameAr,
                    EmployeeName = model.Employees.NameAr,
                    Details = model.Details,
                    DepartmentId = model.DepartmentId ?? 0,
                    DepartmentName = model.Departments.DepartmentNameAr,
                    Description = model.Description,
                    Image = model.Image,
                    ServicesId = model.ServicesId ?? 0,
                    ServicesName = model.Services.ServiceNameAr


                });
            }
            else
            {
                Details = Context.SupportServices.Where(m => m.CustomerId == UserId).Select(model =>
                  new SupportServicesVm
                  {

                      Id = model.Id,
                      CreatedDate = model.CreatedDate,
                      CustomerId = model.CustomerId ?? 0,
                      HourDate = model.HourDate,
                      CreatedBy = model.CreatedBy ?? 0,
                      EmployeeId = model.EmployeeId ?? 0,

                      CustomerName = model.Customers.NameAr,
                      EmployeeName = model.Employees.NameAr,
                      Details = model.Details,
                      DepartmentId = model.DepartmentId ?? 0,
                      DepartmentName = model.Departments.DepartmentNameAr,
                      Description = model.Description,
                      Image = model.Image,
                      ServicesId = model.ServicesId ?? 0,
                      ServicesName = model.Services.ServiceNameAr

                  });
            }
            return Details.ToList();
        }
        public bool Delete(int id)
        {
            Context.Entry(new SupportServices { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }


    }
}