﻿
using Customers.Entities;

namespace Customers.Services
{
    public class BaseService
    {
        private CustomersServiceDBEntities _context;
        private DBHandler _Db;


        protected CustomersServiceDBEntities Context
        {
            get
            {
                return _context ?? (_context = new CustomersServiceDBEntities());
            }
        }

        protected DBHandler db
        {
            get
            {
                return _Db ?? (_Db = new DBHandler());
            }
        }


        protected bool ContextSaveChanges()
        {
            try
            {
                int retVal = Context.SaveChanges();
                if (retVal >= 1)
                    return true;
                else
                    return false;
            }
            catch /*(Exception exception)*/
            {
                return false;
            }
        }
    }
}
