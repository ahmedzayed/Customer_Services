﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
    public class RequestOfferService : BaseService, IRequestOfferService
    {
        public RequestOfferVm GetById(int id)
        {
            IQueryable<RequestOfferVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffers.Where(m => m.Id == id).Select(model =>
                new RequestOfferVm
                {
                    Id = model.Id,
                    RequestId = model.RequestId ?? 0,
                    OfferName = model.PricesOffers.NameAr,
                    OfferId = model.OfferId ?? 0,
                   ActivityName=model.MarketingRequests.CustomerActivities.NameAr,
                   CustName=model.MarketingRequests.Customers.NameAr, 
                   EmployeeName=model.MarketingRequests.Employees.NameAr,
                   RequestStatusName=model.MarketingRequests.CustomerStatus.NameAr,
                    ServiceName = model.MarketingRequests.MarketingRequestsServices.OrderBy(a => a.RequestId).Select(a => a.Services.ServiceNameAr).FirstOrDefault(),

                    OfferSelected = model.OfferSelected ,
                    


                    CreatedDate = model.CreatedDate,

                    
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffers.Where(m => m.Id == id).Select(model =>
                new RequestOfferVm
                {
                    Id = model.Id,
                    RequestId = model.RequestId ?? 0,
                    OfferName = model.PricesOffers.NameAr,
                    OfferId = model.OfferId ?? 0,

                    OfferSelected = model.OfferSelected,
                    ServiceName = model.MarketingRequests.MarketingRequestsServices.OrderBy(a => a.RequestId).Select(a => a.Services.ServiceNameAr).FirstOrDefault(),




                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0,
                     ActivityName = model.MarketingRequests.CustomerActivities.NameAr,
                    CustName = model.MarketingRequests.Customers.NameAr,
                    EmployeeName = model.MarketingRequests.Employees.NameAr,
                    RequestStatusName = model.MarketingRequests.CustomerStatus.NameAr,
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(RequestOfferVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequestsOffers.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsOffers.Add(MarketingRequestsOffers.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

       public int Count()
      {
            return Context.MarketingRequestsOffers.Count();
      }
        public List<RequestOfferVm> FindAll(RequestOfferSm searchModel)
        {
            IQueryable<RequestOfferVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsOffers
                          select new RequestOfferVm
                          {
                              Id = data.Id,
                              RequestId = data.RequestId ?? 0,
                              
                              OfferName = data.PricesOffers.NameAr,
                              OfferId = data.OfferId ?? 0,

                              OfferSelected = data.OfferSelected,



                              CreatedDate = data.CreatedDate,
                              ServiceName=data.MarketingRequests.MarketingRequestsServices.OrderBy(a=>a.RequestId).Select(a=>a.Services.ServiceNameAr).FirstOrDefault(),

                              CreatedBy = data.CreatedBy ?? 0,
                               ActivityName = data.MarketingRequests.CustomerActivities.NameAr,
                              CustName = data.MarketingRequests.Customers.NameAr,
                              EmployeeName = data.MarketingRequests.Employees.NameAr,
                              RequestStatusName = data.MarketingRequests.CustomerStatus.NameAr,
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsOffers
                          select new RequestOfferVm
                          {
                              Id = data.Id,
                              RequestId = data.RequestId ?? 0,
                              OfferName = data.PricesOffers.NameAr,
                              ServiceName = data.MarketingRequests.MarketingRequestsServices.OrderBy(a => a.RequestId).Select(a => a.Services.ServiceNameAr).FirstOrDefault(),

                              OfferId = data.OfferId ?? 0,

                              OfferSelected = data.OfferSelected,



                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0,
                              ActivityName = data.MarketingRequests.CustomerActivities.NameAr,
                              CustName = data.MarketingRequests.Customers.NameAr,
                              EmployeeName = data.MarketingRequests.Employees.NameAr,
                              RequestStatusName = data.MarketingRequests.CustomerStatus.NameAr,

                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsOffers { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}
