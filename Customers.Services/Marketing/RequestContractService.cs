﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
    public class RequestContractService : BaseService, IRequestContractService
    {
        public RequestOfferContractVm GetById(int id)
        {
            IQueryable<RequestOfferContractVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersContracts.Where(m => m.Id == id).Select(model =>
                new RequestOfferContractVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    ContractName = model.Contracts.NameAr,
                    RequestOfferId = model.RequestOfferId ?? 0,
                    CustName = model.MarketingRequestsOffers.MarketingRequests.Customers.NameAr,
                    RequestId = model.MarketingRequestsOffers.MarketingRequests.Id,
                    //ServiceName = model.MarketingRequestsOffers.MarketingRequests.MarketingRequestsServices.OrderBy(a => a.RequestId).Select(a => a.Services.ServiceNameAr).FirstOrDefault(),
                    CustomerId = model.MarketingRequestsOffers.MarketingRequests.CustId ??0,
                    ActivityName = model.MarketingRequestsOffers.MarketingRequests.CustomerActivities.NameAr,
                    EmployeeName=model.MarketingRequestsOffers.MarketingRequests.Employees.NameAr,
                  
                   
                    EndDate = model.EndDate ,
                    StartDate=model.StartDate ,



                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersContracts.Where(m => m.Id == id).Select(model =>
                new RequestOfferContractVm
                {
                    Id = model.Id,
                    ContractId = model.ContractId ?? 0,
                    ContractName = model.Contracts.NameAr,
                    RequestOfferId = model.RequestOfferId ?? 0,
                    CustomerId = model.MarketingRequestsOffers.MarketingRequests.CustId ?? 0,
                    RequestId = model.MarketingRequestsOffers.MarketingRequests.Id,

                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    CustName = model.MarketingRequestsOffers.MarketingRequests.Customers.NameAr,
                    ActivityName = model.MarketingRequestsOffers.MarketingRequests.CustomerActivities.NameAr,
                    EmployeeName = model.MarketingRequestsOffers.MarketingRequests.Employees.NameAr,


                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(RequestOfferContractVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequestsOffersContracts.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsOffersContracts.Add(MarketingRequestsOffersContracts.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public int Count()
        {
            return Context.MarketingRequestsOffersContracts.Count();
        }
        public List<RequestOfferContractVm> GetByCustomerId(int? id)
        {
            IQueryable<RequestOfferContractVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersContracts.Where(m => m.MarketingRequestsOffers.MarketingRequests.CustId == id).Select(model =>
                new RequestOfferContractVm
                {
                    Id = model.Id,
                    ContractName = model.Contracts.NameAr ,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate ,




                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersContracts.Where(m => m.MarketingRequestsOffers.MarketingRequests.CustId == id).Select(model =>
                 new RequestOfferContractVm
                 {
                     Id = model.Id,
                     ContractName = model.Contracts.NameAr,
                     StartDate = model.StartDate,
                     EndDate = model.EndDate,




                     CreatedDate = model.CreatedDate,


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }

        public List<RequestOfferContractVm> FindAll(RequestOfferContractSm searchModel)
        {
            IQueryable<RequestOfferContractVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsOffersContracts
                          select new RequestOfferContractVm
                          {
                              Id = data.Id,
                              ContractId = data.ContractId ?? 0,
                              ContractName = data.Contracts.NameAr,
                              RequestOfferId = data.RequestOfferId ?? 0,
                              CustomerId = data.MarketingRequestsOffers.MarketingRequests.CustId ?? 0,
                              RequestId=data.MarketingRequestsOffers.MarketingRequests.Id,
                              EndDate = data.EndDate,
                              StartDate = data.StartDate,
                              CustName = data.MarketingRequestsOffers.MarketingRequests.Customers.NameAr,
                              ActivityName = data.MarketingRequestsOffers.MarketingRequests.CustomerActivities.NameAr,
                              EmployeeName = data.MarketingRequestsOffers.MarketingRequests.Employees.NameAr,


                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsOffersContracts
                          select new RequestOfferContractVm
                          {
                              Id = data.Id,
                              ContractId = data.ContractId ?? 0,
                              ContractName = data.Contracts.NameAr,
                              RequestOfferId = data.RequestOfferId ?? 0,
                              //ServiceName = data.MarketingRequestsOffers.MarketingRequests.MarketingRequestsServices.OrderBy(a => a.RequestId).Select(a => a.Services.ServiceNameAr).FirstOrDefault(),
                              RequestId = data.MarketingRequestsOffers.MarketingRequests.Id,
                              EndDate = data.EndDate,
                              StartDate = data.StartDate,
                              CustomerId = data.MarketingRequestsOffers.MarketingRequests.CustId ?? 0,


                              CustName = data.MarketingRequestsOffers.MarketingRequests.Customers.NameAr,
                              ActivityName = data.MarketingRequestsOffers.MarketingRequests.CustomerActivities.NameAr,
                              EmployeeName = data.MarketingRequestsOffers.MarketingRequests.Employees.NameAr,


                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0

                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsOffersContracts { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }
    }
}
