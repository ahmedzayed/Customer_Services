﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
  public  class RequestOfferContractService : BaseService, IRequestOfferContractService
    {

        public List<RequestsOffersContractsConditionsVm> GetByRequestOfferId(int? id)
        {
            IQueryable<RequestsOffersContractsConditionsVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.OfferContractId == id).Select(model =>
                new RequestsOffersContractsConditionsVm
                {
                    Id = model.Id,
                    OfferContractId = model.OfferContractId ?? 0,

                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                    



                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.OfferContractId == id).Select(model =>
                 new RequestsOffersContractsConditionsVm
                 {
                     Id = model.Id,
                     OfferContractId = model.OfferContractId ?? 0,

                     ConditionId = model.ConditionId ?? 0,
                     ConditionName = model.ConditionsOffer.NameAr,




                     CreatedDate = model.CreatedDate,


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }
        public List<RequestsOffersContractsConditionsVm> GetByCustomerId(int? id)
        {
            IQueryable<RequestsOffersContractsConditionsVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.MarketingRequestsOffersContracts.MarketingRequestsOffers.MarketingRequests.CustId == id).Select(model =>
                new RequestsOffersContractsConditionsVm
                {
                    Id = model.Id,
                    OfferContractId = model.OfferContractId ?? 0,

                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,




                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.MarketingRequestsOffersContracts.MarketingRequestsOffers.MarketingRequests.CustId == id).Select(model =>
                 new RequestsOffersContractsConditionsVm
                 {
                     Id = model.Id,
                     OfferContractId = model.OfferContractId ?? 0,

                     ConditionId = model.ConditionId ?? 0,
                     ConditionName = model.ConditionsOffer.NameAr,




                     CreatedDate = model.CreatedDate,


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }
       
        public RequestsOffersContractsConditionsVm GetById(int id)
        {
            IQueryable<RequestsOffersContractsConditionsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.Id == id).Select(model =>
                new RequestsOffersContractsConditionsVm
                {
                    Id = model.Id,
                    OfferContractId = model.OfferContractId ?? 0,

                   
                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                 



                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersContractsConditions.Where(m => m.Id == id).Select(model =>
                new RequestsOffersContractsConditionsVm
                {
                    Id = model.Id,
                    OfferContractId = model.OfferContractId ?? 0,

                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                    
                    CreatedDate = model.CreatedDate,
                    
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(RequestsOffersContractsConditionsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequestsOffersContractsConditions.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsOffersContractsConditions.Add(MarketingRequestsOffersContractsConditions.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public List<RequestsOffersContractsConditionsVm> FindAll(RequestsOffersContractsConditionsSm searchModel)
        {
            IQueryable<RequestsOffersContractsConditionsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsOffersContractsConditions
                          select new RequestsOffersContractsConditionsVm
                          {
                              Id = data.Id,
                              OfferContractId = data.OfferContractId ?? 0,

                              ConditionId = data.ConditionId ?? 0,
                              ConditionName = data.ConditionsOffer.NameAr,




                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsOffersContractsConditions
                          select new RequestsOffersContractsConditionsVm
                          {
                              Id = data.Id,
                              OfferContractId = data.OfferContractId ?? 0,

                              ConditionId = data.ConditionId ?? 0,
                              ConditionName = data.ConditionsOffer.NameAr,




                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0

                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsOffersContractsConditions { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

        
    }
}
