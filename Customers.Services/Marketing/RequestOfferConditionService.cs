﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
  public  class RequestOfferConditionService :BaseService,IRequestOfferConditionService
    {
        public List<RequestOfferConditionVm> GetByRequestOfferId(int? id)
        {
            IQueryable<RequestOfferConditionVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersConditions.Where(m => m.RequestOfferId == id).Select(model =>
                new RequestOfferConditionVm
                {
                    Id = model.Id,
                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                    RequestOfferId = model.RequestOfferId ?? 0,




                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersConditions.Where(m => m.RequestOfferId == id).Select(model =>
                 new RequestOfferConditionVm
                 {
                     Id = model.Id,
                     ConditionId = model.ConditionId ?? 0,
                     ConditionName = model.ConditionsOffer.NameAr,
                     RequestOfferId = model.RequestOfferId ?? 0,




                     CreatedDate = model.CreatedDate,


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }

        public RequestOfferConditionVm GetById(int id)
        {
            IQueryable<RequestOfferConditionVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsOffersConditions.Where(m => m.Id == id).Select(model =>
                new RequestOfferConditionVm
                {
                    Id = model.Id,
                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                    RequestOfferId = model.RequestOfferId ?? 0,
                 



                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsOffersConditions.Where(m => m.Id == id).Select(model =>
                new RequestOfferConditionVm
                {
                    Id = model.Id,
                    ConditionId = model.ConditionId ?? 0,
                    ConditionName = model.ConditionsOffer.NameAr,
                    RequestOfferId = model.RequestOfferId ?? 0,




                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(RequestOfferConditionVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequestsOffersConditions.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsOffersConditions.Add(MarketingRequestsOffersConditions.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public List<RequestOfferConditionVm> FindAll(RequestOfferConditionSm searchModel)
        {
            IQueryable<RequestOfferConditionVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsOffersConditions
                          select new RequestOfferConditionVm
                          {
                              Id = data.Id,
                              ConditionId = data.ConditionId ?? 0,
                              ConditionName = data.ConditionsOffer.NameAr,
                              RequestOfferId = data.RequestOfferId ?? 0,




                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsOffersConditions
                          select new RequestOfferConditionVm
                          {
                              Id = data.Id,
                              ConditionId = data.ConditionId ?? 0,
                              ConditionName = data.ConditionsOffer.NameAr,
                              RequestOfferId = data.RequestOfferId ?? 0,




                              CreatedDate = data.CreatedDate,


                              CreatedBy = data.CreatedBy ?? 0
                          };

            
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsOffersConditions { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

       
    }
}
