﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
  public  class RequestStatusService : BaseService, IRequestStatusService
    {

        public List<RequestStatusVm> GetServicesByRequestId(int? id)
        {
            IQueryable<RequestStatusVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.RequestId == id).Select(model =>
                new RequestStatusVm
                {
                    Id = model.Id,
                    StatusId = model.StatusId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    StatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.RequestId == id).Select(model =>
                 new RequestStatusVm
                 {
                     Id = model.Id,
                     StatusId = model.StatusId ?? 0,
                     RequestId = model.RequestId ?? 0,
                     StatusName = model.CustomerStatus.NameAr,


                     CreatedDate = model.CreatedDate,
                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }


        public RequestStatusVm GetById(int id)
        {
            IQueryable<RequestStatusVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.Id == id).Select(model =>
                new RequestStatusVm
                {
                    Id = model.Id,
                    StatusId = model.StatusId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    StatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.Id == id).Select(model =>
                new RequestStatusVm
                {
                    Id = model.Id,
                    StatusId = model.StatusId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    StatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }
        public RequestStatusVm GetByRequestId(int id)
        {
            IQueryable<RequestStatusVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.RequestId == id).Select(model =>
                new RequestStatusVm
                {
                    Id = model.Id,
                    StatusId = model.StatusId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    StatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsStatus.Where(m => m.RequestId == id).Select(model =>
                new RequestStatusVm
                {
                    Id = model.Id,
                    StatusId = model.StatusId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    StatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }


        public bool Save(RequestStatusVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Status = Context.Entry(MarketingRequestsStatus.Clone(viewModel));
                Status.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsStatus.Add(MarketingRequestsStatus.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<RequestStatusVm> FindAll(RequestStatusSm searchModel)
        {
            IQueryable<RequestStatusVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsStatus
                          select new RequestStatusVm
                          {
                              Id = data.Id,
                              StatusId = data.StatusId ?? 0,
                              StatusName = data.CustomerStatus.NameAr,
                              CreatedDate = data.CreatedDate,
                              CreatedBy = data.CreatedBy,
                              RequestId = data.RequestId ?? 0,

                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsStatus
                          select new RequestStatusVm
                          {
                              Id = data.Id,
                              StatusId = data.StatusId ?? 0,
                              StatusName = data.CustomerStatus.NameAr,
                              CreatedDate = data.CreatedDate,
                              CreatedBy = data.CreatedBy,
                              RequestId = data.RequestId ?? 0,
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsStatus { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}