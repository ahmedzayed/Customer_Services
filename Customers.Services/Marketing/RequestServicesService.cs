﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
  public  class RequestServicesService : BaseService, IRequestServicesService
    {

        public List<RequestsServicesVm> GetServicesByRequestId(int? id)
        {
            IQueryable<RequestsServicesVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsServices.Where(m => m.RequestId == id).Select(model =>
                new RequestsServicesVm
                {
                    Id = model.Id,
                    ServiceId = model.ServiceId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    ServiceName = model.Services.ServiceNameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsServices.Where(m => m.RequestId == id).Select(model =>
                 new RequestsServicesVm
                 {
                     Id = model.Id,
                     ServiceId = model.ServiceId ?? 0,
                     RequestId = model.RequestId ?? 0,
                     ServiceName = model.Services.ServiceNameAr,


                     CreatedDate = model.CreatedDate,
                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }


        public List<RequestsServicesVm> GetServicesByCustomerId(int? id)
        {
            IQueryable<RequestsServicesVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsServices.Where(m => m.MarketingRequests.CustId == id).Select(model =>
                new RequestsServicesVm
                {
                    Id = model.Id,
                    ServiceId = model.ServiceId ?? 0,
                    RequestId = model.RequestId ?? 0,
                    ServiceName = model.Services.ServiceNameAr,


                    CreatedDate = model.CreatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsServices.Where(m => m.MarketingRequests.CustId == id).Select(model =>
                 new RequestsServicesVm
                 {
                     Id = model.Id,
                     ServiceId = model.ServiceId ?? 0,
                     RequestId = model.RequestId ?? 0,
                     ServiceName = model.Services.ServiceNameAr,


                     CreatedDate = model.CreatedDate,
                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }
        public RequestsServicesVm GetById(int id)
        {
            IQueryable<RequestsServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequestsServices.Where(m => m.Id == id).Select(model =>
                new RequestsServicesVm
                {
                    Id = model.Id,
                    ServiceId = model.ServiceId ?? 0,
                    ServiceName = model.Services.ServiceNameAr,
                    RequestId = model.RequestId ?? 0,
                  


                    CreatedDate = model.CreatedDate,

                  
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequestsServices.Where(m => m.Id == id).Select(model =>
                new RequestsServicesVm
                {
                    Id = model.Id,
                    ServiceId = model.ServiceId ?? 0,
                    ServiceName = model.Services.ServiceNameAr,
                    RequestId = model.RequestId ?? 0,



                    CreatedDate = model.CreatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public int FindLast()
        {
            int? Id = (
         from p in Context.MarketingRequests
         orderby p.Id descending
         select p.Id
     ).Take(1).SingleOrDefault();

            return (int)Id;
        }
        public bool Save(RequestsServicesVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequestsServices.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequestsServices.Add(MarketingRequestsServices.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<RequestsServicesVm> FindAll(RequestsServicesSm searchModel)
        {
            IQueryable<RequestsServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequestsServices
                          select new RequestsServicesVm
                          {
                              Id = data.Id,
                              ServiceId = data.ServiceId ?? 0,
                              ServiceName = data.Services.ServiceNameAr,
                              CreatedDate = data.CreatedDate,
                              CreatedBy = data.CreatedBy,
                              RequestId = data.RequestId ??0,
                             
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequestsServices
                          select new RequestsServicesVm
                          {
                              Id = data.Id,
                              ServiceId = data.ServiceId ?? 0,
                              ServiceName = data.Services.ServiceNameAr,
                              CreatedDate = data.CreatedDate,
                              CreatedBy = data.CreatedBy,
                              RequestId = data.RequestId ?? 0,
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequestsServices { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}