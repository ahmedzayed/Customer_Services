﻿using Customers.Abstracts.Marketing;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.Marketing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Marketing
{
    public class RequestService : BaseService, IRequestService
    {
        public int Count()
        {
            return Context.MarketingRequests.Count();
        }
        public MarketingVm GetById(int id)
        {
            IQueryable<MarketingVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequests.Where(m => m.Id == id).Select(model =>
                new MarketingVm
                {
                    Id = model.Id,
                    CustId = model.CustId ??0,
                    CustName = model.Customers.NameAr,
                    ActivityId = model.ActivityId ??0,
                    ActivityName = model.CustomerActivities.NameAr,
                    EmployeeId = model.EmployeeId,
                    EmployeeName = model.Employees.NameAr,
                    Note = model.Note,
                    RequestStatusId = model.RequestStatusId ??0,
                    RequestStatusName = model.CustomerStatus.NameAr,


                    CreatedDate = model.CreatedDate,
                    
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.MarketingRequests.Where(m => m.Id == id).Select(model =>
                new MarketingVm
                {
                    Id = model.Id,
                    CustId = model.CustId ??0,
                    CustName = model.Customers.NameAr,
                    ActivityId = model.ActivityId ??0,
                    ActivityName = model.CustomerActivities.NameAr,
                    EmployeeId = model.EmployeeId,
                    EmployeeName = model.Employees.NameAr,
                    Note = model.Note,
                    RequestStatusId = model.RequestStatusId ??0,
                    RequestStatusName = model.CustomerStatus.NameAr,

                    CreatedDate = model.CreatedDate,

                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save( MarketingVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(MarketingRequests.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.MarketingRequests.Add(MarketingRequests.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<MarketingVm> GetByEmployeeId(int id)
        {
            IQueryable<MarketingVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.MarketingRequests.Where(m => m.EmployeeId == id).Select(data =>
                new MarketingVm
                {
                    Id = data.Id,
                    CustId = data.CustId ?? 0,
                    CustName = data.Customers.NameAr,
                    CreatedDate = data.CreatedDate,
                    EmployeeId = data.EmployeeId,
                    //Time = data.Time,
                    UpdatedBy = data.UpdatedBy,
                    UpdatedDate = data.UpdatedDate,
                    CreatedBy = data.CreatedBy ?? 0,
                    ActivityId = data.ActivityId ?? 0,
                    ActivityName = data.CustomerActivities.NameAr,
                    EmployeeName = data.Employees.NameAr,
                    Note = data.Note,
                    RequestStatusId = data.RequestStatusId ?? 0,
                    RequestStatusName = data.CustomerStatus.NameAr
                });
            }
            else
            {
                Details = Context.MarketingRequests.Where(m => m.EmployeeId == id).Select(data =>
               new MarketingVm
               {
                     Id = data.Id,
                     CustId = data.CustId ?? 0,
                     CustName = data.Customers.NameAr,
                     CreatedDate = data.CreatedDate,
                     EmployeeId = data.EmployeeId,
                     //Time = data.Time,
                     UpdatedBy = data.UpdatedBy,
                     UpdatedDate = data.UpdatedDate,
                     CreatedBy = data.CreatedBy ?? 0,
                     ActivityId = data.ActivityId ?? 0,
                     ActivityName = data.CustomerActivities.NameAr,
                     EmployeeName = data.Employees.NameAr,
                     Note = data.Note,
                     RequestStatusId = data.RequestStatusId ?? 0,
                     RequestStatusName = data.CustomerStatus.NameAr
                 });
            }
            return Details.ToList();
        }
        public List<MarketingVm> FindAll(MarketingSm searchModel)
        {
            IQueryable<MarketingVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.MarketingRequests
                          select new MarketingVm
                          {
                              Id = data.Id,
                              CustId = data.CustId ??0,
                              CustName = data.Customers.NameAr,
                              CreatedDate = data.CreatedDate,
                              EmployeeId = data.EmployeeId,
                              //Time = data.Time,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0,
                              ActivityId=data.ActivityId ??0,
                              ActivityName=data.CustomerActivities.NameAr,
                              EmployeeName = data.Employees.NameAr,
                              Note =data.Note,
                              RequestStatusId=data.RequestStatusId ??0,
                              RequestStatusName = data.CustomerStatus.NameAr
                          };
            }
            else
            {
                Details = from data in Context.MarketingRequests
                          select new MarketingVm
                          {
                              Id = data.Id,
                              CustId = data.CustId ??0,
                              CustName = data.Customers.NameAr,
                              CreatedDate = data.CreatedDate,
                              EmployeeId = data.EmployeeId,
                              //Time = data.Time,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0,
                              ActivityId = data.ActivityId ??0,
                              ActivityName = data.CustomerActivities.NameAr,
                              //EmployeeName=data.
                              Note = data.Note,
                              RequestStatusId = data.RequestStatusId ??0,
                              //RequestStatusName=data.
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new MarketingRequests { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }




    }
}