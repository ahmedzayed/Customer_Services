﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Customers.Abstracts;
using Customers.Services;
using Customers.Model;
using Customers.Helpers.Utilities;

namespace Bank.Services
{
    public class LookupService : BaseService, ILookupService
    {

        #region BasicInput
        public List<KeyValueLookup> FindCustomerActivities()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  النشاط --" };
                Items = Context.CustomerActivities.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Activity  --" };
                Items = Context.CustomerActivities.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindServices()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الخدمه --" };
                Items = Context.Services.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.ServiceNameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Services  --" };
                Items = Context.Services.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.ServiceNameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindWorkSystem()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  نظام العمل --" };
                Items = Context.WorkSystem.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select WorkSystem  --" };
                Items = Context.WorkSystem.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindEmployeeType()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   نوع الموظف --" };
                Items = Context.EmployeeType.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.Type
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select EmployeeType  --" };
                Items = Context.EmployeeType.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.Type
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindSpecialization()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   التخصص --" };
                Items = Context.EmployeeSpecializations.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Specialization  --" };
                Items = Context.EmployeeSpecializations.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindItems()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  البند --" };
                Items = Context.Items.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Items  --" };
                Items = Context.Items.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindCustomerAnalyses()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  التحليل --" };
                Items = Context.CustomerAnalyses.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Analyses  --" };
                Items = Context.CustomerAnalyses.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindConditions()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الشرط --" };
                Items = Context.ConditionsOffer.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Items  --" };
                Items = Context.ConditionsOffer.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindDepartments()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  القسم --" };
                Items = Context.Departments.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.DepartmentNameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Department  --" };
                Items = Context.Departments.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.DepartmentNameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindConditionsEmp()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الشرط --" };
                Items = Context.ConditionEmp.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Conditions  --" };
                Items = Context.ConditionEmp.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        #endregion


        public List<KeyValueLookup> FindEngineeringEmp()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  المهندس --" };
                Items = Context.Employees.Where(a=>a.EmployeeTypeId==2).Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Engineering  --" };
                Items = Context.Employees.Where(a => a.EmployeeTypeId == 2).Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        #region Marketing
        public List<KeyValueLookup> FindEmployees()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الموظف --" };
                Items = Context.Employees.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Employee  --" };
                Items = Context.Employees.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindCustomers()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  العميل --" };
                Items = Context.Customers.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Customer  --" };
                Items = Context.Customers.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindCustomerStatus()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  حاله العميل --" };
                Items = Context.CustomerStatus.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Customer Status  --" };
                Items = Context.CustomerStatus.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindOffer()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   العرض --" };
                Items = Context.PricesOffers.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Offer  --" };
                Items = Context.PricesOffers.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindContract()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   العقد --" };
                Items = Context.Contracts.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select contract  --" };
                Items = Context.Contracts.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        #endregion
    }
}
