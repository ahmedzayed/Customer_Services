﻿using Customers.Abstracts.BasicInput;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.BasicInput
{
  public   class PriceOffersCondionsService : BaseService, IPriceOffersConditionsService
    {
        public PriceOffersConditionVm GetById(int id)
        {
            IQueryable<PriceOffersConditionVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.PriceOffersConditions.Where(m => m.Id == id).Select(model =>
                new PriceOffersConditionVm
                {
                    Id = model.Id,
                    PriceOffersId = model.PriceOffersId,
                    ConditionsId = model.ConditionsId,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.PriceOffersConditions.Where(m => m.Id == id).Select(model =>
                new PriceOffersConditionVm
                {
                    Id = model.Id,

                    PriceOffersId = model.PriceOffersId,
                    ConditionsId = model.ConditionsId,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public List<PriceOffersConditionVm> GetByPriceOffersId(int? id)
        {
            IQueryable<PriceOffersConditionVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.PriceOffersConditions.Where(m => m.PriceOffersId == id).Select(model =>
                new PriceOffersConditionVm
                {
                    Id = model.Id,
                    PriceOffersId = model.PriceOffersId,
                    ConditionsId = model.ConditionsId,
                    ConditionsName=model.ConditionsOffer.NameAr,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.PriceOffersConditions.Where(m => m.PriceOffersId == id).Select(model =>
                 new PriceOffersConditionVm
                 {
                     Id = model.Id,
                     PriceOffersId = model.PriceOffersId,
                     ConditionsId = model.ConditionsId,



                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }
        public bool Save(PriceOffersConditionVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(PriceOffersConditions.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.PriceOffersConditions.Add(PriceOffersConditions.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<PriceOffersConditionVm> FindAll(PriceOffersConditionSm searchModel)
        {
            IQueryable<PriceOffersConditionVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.PriceOffersConditions
                          select new PriceOffersConditionVm
                          {
                              Id = data.Id,
                              PriceOffersId = data.PriceOffersId,
                              ConditionsId = data.ConditionsId,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.PriceOffersConditions
                          select new PriceOffersConditionVm
                          {
                              Id = data.Id,
                              PriceOffersId = data.PriceOffersId,
                              ConditionsId = data.ConditionsId,


                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.PriceOffersConditions { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }








    }
}