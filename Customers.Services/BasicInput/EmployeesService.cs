﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Entities;

namespace Customers.Services.BasicInput
{
   public class EmployeesService : BaseService, IEmployeesService
    {
        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.Employees { Id = id }).State = System.Data.Entity.EntityState.Deleted;
            return ContextSaveChanges();
        }

        public List<EmployeesVm> FindAll(EmployeesSm searchModel)
        {
            IQueryable<EmployeesVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = from data in Context.Employees
                          select new EmployeesVm
                          {
                              Id = data.Id,
                              NameAr = data.NameAr,
                              NameEn = data.NameEn,
                              WorkSystemId = data.WorkSystemId ?? 0,
                              CreatedDate = data.CreatedDate,
                              DateOfAppointment = data.DateOfAppointment,
                              FaceNumber = data.FaceNumber,
                              Nationality = data.Nationality,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              Salary = data.Salary,
                              SpecializationsId=data.SpecializationsId ??0,
                              WorkHours=data.WorkHours,
                              WorkLocation=data.WorkLocation,
                              EmployeeTypeId=data.EmployeeTypeId ??0,
                              EmployeeTypeName=data.EmployeeType.Type,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.Employees
                          select new EmployeesVm
                          {
                              Id = data.Id,
                              NameAr = data.NameAr,
                              NameEn = data.NameEn,
                              WorkSystemId = data.WorkSystemId ?? 0,
                              CreatedDate = data.CreatedDate,
                              DateOfAppointment = data.DateOfAppointment,
                              FaceNumber = data.FaceNumber,
                              Nationality = data.Nationality,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              Salary = data.Salary,
                              SpecializationsId = data.SpecializationsId ?? 0,
                              WorkHours = data.WorkHours,
                              WorkLocation = data.WorkLocation,
                              EmployeeTypeId = data.EmployeeTypeId ?? 0,
                              EmployeeTypeName = data.EmployeeType.Type,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public int FindLast()
        {
            int? Id = (
         from p in Context.Employees
         orderby p.Id descending
         select p.Id
     ).Take(1).SingleOrDefault();

            return (int)Id;
        }
     

        public EmployeesVm GetById(int id)
        {
            IQueryable<EmployeesVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.Employees.Where(m => m.Id == id).Select(model =>
                new EmployeesVm
                {
                    Id = model.Id,
                    NameAr = model.NameAr,
                    NameEn = model.NameEn,
                    WorkSystemId = model.WorkSystemId ?? 0,
                    CreatedDate = model.CreatedDate,
                    DateOfAppointment = model.DateOfAppointment,
                    FaceNumber = model.FaceNumber,
                    Nationality = model.Nationality,
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,
                    Salary = model.Salary,
                    EmployeeTypeId = model.EmployeeTypeId ?? 0,
                    EmployeeTypeName = model.EmployeeType.Type,
                    SpecializationsId = model.SpecializationsId ?? 0,
                    WorkHours = model.WorkHours,
                    WorkLocation = model.WorkLocation,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Employees.Where(m => m.Id == id).Select(model =>
                 new EmployeesVm
                 {
                     Id = model.Id,
                     NameAr = model.NameAr,
                     NameEn = model.NameEn,
                     WorkSystemId = model.WorkSystemId ?? 0,
                     CreatedDate = model.CreatedDate,
                     DateOfAppointment = model.DateOfAppointment,
                     FaceNumber = model.FaceNumber,
                     Nationality = model.Nationality,
                     UpdatedBy = model.UpdatedBy,
                     UpdatedDate = model.UpdatedDate,
                     Salary = model.Salary,
                     SpecializationsId = model.SpecializationsId ?? 0,
                     WorkHours = model.WorkHours,
                     WorkLocation = model.WorkLocation,

                     CreatedBy = model.CreatedBy ?? 0,
                     EmployeeTypeId = model.EmployeeTypeId ?? 0,
                     EmployeeTypeName = model.EmployeeType.Type
                 });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(EmployeesVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var SaveChange = Context.Entry(Employees.Clone(viewModel));
                SaveChange.State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                Context.Employees.Add(Customers.Entities.Employees.Clone(viewModel));
            }
            return ContextSaveChanges();
        }



    }
}