﻿using Customers.Abstracts.BasicInput;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.BasicInput
{
    public class EmployeeSpecializationsService : BaseService, IEmployeeSpecializationsService
    {
        public BasicInputVm GetById(int id)
        {
            IQueryable<BasicInputVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.EmployeeSpecializations.Where(m => m.Id == id).Select(model =>
                new BasicInputVm
                {
                    Id = model.Id,
                    NameAr = model.NameAr,
                    NameEn = model.NameEn,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.EmployeeSpecializations.Where(m => m.Id == id).Select(model =>
                new BasicInputVm
                {
                    Id = model.Id,
                    NameAr = model.NameAr,
                    NameEn = model.NameEn,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(BasicInputVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var erpBrand = Context.Entry(EmployeeSpecializations.Clone(viewModel));
                erpBrand.State = EntityState.Modified;
            }
            else
            {
                Context.EmployeeSpecializations.Add(EmployeeSpecializations.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<BasicInputVm> FindAll(BasicInputSm searchModel)
        {
            IQueryable<BasicInputVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.EmployeeSpecializations
                          select new BasicInputVm
                          {
                              Id = data.Id,
                              Name = data.NameAr,
                              NameAr = data.NameAr,
                              NameEn = data.NameEn,
                          };
            }
            else
            {
                Details = from data in Context.EmployeeSpecializations
                          select new BasicInputVm
                          {
                              Id = data.Id,
                              Name = data.NameAr,
                              NameAr = data.NameAr,
                              NameEn = data.NameEn,
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new EmployeeSpecializations { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }
    }
}
