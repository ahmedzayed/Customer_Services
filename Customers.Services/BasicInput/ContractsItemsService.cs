﻿using Customers.Helpers.Utilities;
using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Abstracts.BasicInput;

namespace Customers.Services.BasicInput
{
   public class ContractsItemsService : BaseService, IContractsItemsService
    {
        public ContractsItemsVm GetById(int id)
        {
            IQueryable<ContractsItemsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.ContractsItems.Where(m => m.Id == id).Select(model =>
                new ContractsItemsVm
                {
                    Id = model.Id,
                    ContractsId = model.ContractsId,
                    ItemsId = model.ItemsId,
                  
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.ContractsItems.Where(m => m.Id == id).Select(model =>
                new ContractsItemsVm
                {
                    Id = model.Id,
                    
                    ContractsId = model.ContractsId,
                    ItemsId = model.ItemsId,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public List<ContractsItemsVm> GetByContractsId(int? id)
        {
            IQueryable<ContractsItemsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.ContractsItems.Where(m => m.ContractsId == id).Select(model =>
                new ContractsItemsVm
                {
                    Id = model.Id,
                    ContractsId = model.ContractsId,
                    ItemsId = model.ItemsId,
                    ItemsName=model.Items.NameAr,
                   


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.ContractsItems.Where(m => m.ContractsId == id).Select(model =>
                 new ContractsItemsVm
                 {
                     Id = model.Id,
                     ContractsId = model.ContractsId,
                     ItemsId = model.ItemsId,
                     


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }
        public bool Save(ContractsItemsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(Customers.Entities.ContractsItems.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.ContractsItems.Add(Customers.Entities.ContractsItems.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<ContractsItemsVm> FindAll(ContractsItemsSm searchModel)
        {
            IQueryable<ContractsItemsVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.ContractsItems
                          select new ContractsItemsVm
                          {
                              Id = data.Id,
                            ContractsId=data.ContractsId,
                            ItemsId=data.ItemsId,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.ContractsItems
                          select new ContractsItemsVm
                          {
                              Id = data.Id,
                             ContractsId=data.ContractsId,
                             ItemsId=data.ItemsId,

                              
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.ContractsItems { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

       

        

        

        
    }
}