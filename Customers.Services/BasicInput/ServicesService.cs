﻿using Customers.Abstracts.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Helpers.Utilities;
using System.Data.Entity;
using Customers.Model.BasicInput;

namespace Customers.Services.BasicInput
{
  public  class ServicesService : BaseService, IServicesService
    {
        public ServicesVm GetById(int id)
        {
            IQueryable<ServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Services.Where(m => m.Id == id).Select(model =>
                new ServicesVm
                {
                    Id = model.Id,
                    ServiceNameAr = model.ServiceNameAr,
                    ServiceNameEn = model.ServiceNameEn,
                    CreatedDate=model.CreatedDate,
                    Price=model.Price,
                    Time=model.Time,
                    UpdatedBy=model.UpdatedBy,
                    UpdatedDate=model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Services.Where(m => m.Id == id).Select(model =>
                new ServicesVm
                {
                    Id = model.Id,
                    ServiceNameAr = model.ServiceNameAr,
                    ServiceNameEn = model.ServiceNameEn,
                    CreatedDate = model.CreatedDate,
                    Price = model.Price,
                    Time = model.Time,
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(ServicesVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(Customers.Entities.Services.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.Services.Add(Customers.Entities.Services.Clone(viewModel));
            }
            return ContextSaveChanges();
        }

        public List<ServicesVm> FindAll(ServicesSm searchModel)
        {
            IQueryable<ServicesVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.Services
                          select new ServicesVm
                          {
                              Id = data.Id,
                              ServiceNameAr = data.ServiceNameAr,
                              ServiceNameEn = data.ServiceNameEn,
                              CreatedDate = data.CreatedDate,
                              Price = data.Price,
                              Time = data.Time,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.Services
                          select new ServicesVm
                          {
                              Id = data.Id,
                              ServiceNameAr = data.ServiceNameAr,
                              ServiceNameEn = data.ServiceNameEn,
                              CreatedDate = data.CreatedDate,
                              Price = data.Price,
                              Time = data.Time,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,
                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.Services { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

      
        
      
    }
}
