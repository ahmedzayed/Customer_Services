﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Entities;

namespace Customers.Services.BasicInput
{
   public  class EmployeesConditionService : BaseService, IEmployeesConditionService
    {
        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.EmployeesConditions { Id = id }).State = System.Data.Entity.EntityState.Deleted;
            return ContextSaveChanges();
        }

        public List<EmployeesConditionsVm> FindAll(EmployeesConditionsSm searchModel)
        {
            IQueryable<EmployeesConditionsVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = from data in Context.EmployeesConditions
                          select new EmployeesConditionsVm
                          {
                              Id = data.Id,
                              CondionId = data.ConditionId ??0,
                              EmployeeId = data.EmployeeId ??0,
                             

                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.EmployeesConditions
                          select new EmployeesConditionsVm
                          {
                              Id = data.Id,
                              CondionId = data.ConditionId ?? 0,
                              EmployeeId = data.EmployeeId ?? 0,


                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }




        public List<EmployeesConditionsVm> GetByEmployeeId(int? id)
        {
            IQueryable<EmployeesConditionsVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.EmployeesConditions.Where(m => m.EmployeeId == id).Select(model =>
                new EmployeesConditionsVm
                {
                    Id = model.Id,
                    CondionId = model.ConditionId ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,
                    ConditionName=model.ConditionEmp.NameAr,
                    EmployeeName=model.Employees.NameAr,

                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.EmployeesConditions.Where(m => m.EmployeeId == id).Select(model =>
                 new EmployeesConditionsVm
                 {
                     Id = model.Id,
                     CondionId = model.ConditionId ?? 0,
                     EmployeeId = model.EmployeeId ?? 0,


                     UpdatedBy = model.UpdatedBy,
                     UpdatedDate = model.UpdatedDate,

                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }


        public bool Save(EmployeesConditionsVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var SaveChange = Context.Entry(EmployeesConditions.Clone(viewModel));
                SaveChange.State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                Context.EmployeesConditions.Add(Customers.Entities.EmployeesConditions.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public EmployeesConditionsVm GetById(int id)
        {
            IQueryable<EmployeesConditionsVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.EmployeesConditions.Where(m => m.Id == id).Select(model =>
                new EmployeesConditionsVm
                {
                    Id = model.Id,
                    CondionId = model.ConditionId ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,


                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.EmployeesConditions.Where(m => m.Id == id).Select(model =>
                new EmployeesConditionsVm
                {
                    Id = model.Id,
                    CondionId = model.ConditionId ?? 0,
                    EmployeeId = model.EmployeeId ?? 0,


                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }
    }
}