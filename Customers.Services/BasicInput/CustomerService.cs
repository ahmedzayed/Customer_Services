﻿using Customers.Abstracts.BasicInput;
using Customers.Entities;
using Customers.Helpers.Utilities;
using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.BasicInput
{
    public class CustomerService : BaseService, ICustomerService
    {
        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.Customers { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

        public List<CustomerVm> FindAll(CustomerSm searchModel)
        {
            IQueryable<CustomerVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.Customers
                          select new CustomerVm
                          {
                              Id = data.Id,
                              NameAr = data.NameAr,
                              NameEn = data.NameEn,
                              EmployeeName = data.MarketingRequests.OrderBy(a => a.CustId).Select(a => a.Employees.NameAr).FirstOrDefault(),
                              CustomerActivityId = data.CustomerActivityId ?? 0,
                              CreatedDate = data.CreatedDate,
                              CustomerAddress = data.CustomerAddress,
                              CustomerWebSite = data.CustomerWebSite,
                              Details = data.Details,

                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.Customers
                          select new CustomerVm
                          {
                              Id = data.Id,
                             NameAr=data.NameAr,
                              NameEn = data.NameEn,
                              EmployeeName = data.MarketingRequests.OrderBy(a => a.CustId).Select(a => a.Employees.NameAr).FirstOrDefault(),
                             
                              CustomerActivityId = data.CustomerActivityId ?? 0,
                              CreatedDate = data.CreatedDate,
                              CustomerAddress = data.CustomerAddress,
                              CustomerWebSite = data.CustomerWebSite,
                              Details = data.Details,
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }

        public int FindLast()
        {
            int? Id = (
         from p in Context.Customers
         orderby p.Id descending
         select p.Id
     ).Take(1).SingleOrDefault();

            return (int)Id;
        }
       

        public CustomerVm GetById(int id)
        {
            IQueryable<CustomerVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.Customers.Where(m => m.Id == id).Select(model =>
                new CustomerVm
                {
                    Id = model.Id,
                    NameAr = model.NameAr,
                    NameEn = model.NameEn,
                    CustomerActivityId = model.CustomerActivityId ?? 0,
                    CreatedDate = model.CreatedDate,
                    CustomerAddress = model.CustomerAddress,
                    CustomerWebSite = model.CustomerWebSite,
                    Details = model.Details,
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.Customers.Where(m => m.Id == id).Select(model =>
                 new CustomerVm
                 {
                     Id = model.Id,
                     NameAr = model.NameAr,
                     NameEn = model.NameEn,
                     CustomerActivityId = model.CustomerActivityId ?? 0,
                     CreatedDate = model.CreatedDate,
                     CustomerAddress = model.CustomerAddress,
                     CustomerWebSite = model.CustomerWebSite,
                     Details = model.Details,
                     UpdatedBy = model.UpdatedBy,
                     UpdatedDate = model.UpdatedDate,

                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.FirstOrDefault();
        }

        public bool Save(CustomerVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var SaveChange = Context.Entry(Customers.Entities.Customers.Clone(viewModel));
                SaveChange.State = EntityState.Modified;
            }
            else
            {
                Context.Customers.Add(Customers.Entities.Customers.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


    
    }
}