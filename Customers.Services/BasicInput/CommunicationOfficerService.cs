﻿using Customers.Abstracts.BasicInput;
using Customers.Helpers.Utilities;
using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.BasicInput
{
  public  class CommunicationOfficerService : BaseService, ICommunicationOfficerService
    {
        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.CommunicationOfficer { Id = id }).State = EntityState.Deleted;
            return ContextSaveChanges();
        }

        public List<CommunicationOfficerVm> FindAll(CommunicationOfficerSm searchModel)
        {
            IQueryable<CommunicationOfficerVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = from data in Context.CommunicationOfficer
                          select new CommunicationOfficerVm
                          {
                              Id = data.Id,
                              Name = data.Name,
                              JobeName = data.JobName,
                              CustomerId = data.CustomerId ?? 0,
                              PhoneNumber = data.PhoneNumber,
                              CreatedDate = data.CreatedDate,
                              Email = data.Email,
                             
                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.CommunicationOfficer
                          select new CommunicationOfficerVm
                          {
                              Id = data.Id,
                              Name = data.Name,
                              JobeName = data.JobName,
                              CustomerId = data.CustomerId ?? 0,
                              PhoneNumber = data.PhoneNumber,
                              CreatedDate = data.CreatedDate,
                              Email = data.Email,

                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }




        public List<CommunicationOfficerVm> GetByCustomerId(int? id)
        {
            IQueryable<CommunicationOfficerVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.CommunicationOfficer.Where(m => m.CustomerId == id).Select(model =>
                new CommunicationOfficerVm
                {
                    Id = model.Id,
                    Name = model.Name,
                    JobeName = model.JobName,
                    CustomerId = model.CustomerId ?? 0,
                    PhoneNumber = model.PhoneNumber,
                    CreatedDate = model.CreatedDate,
                    Email = model.Email,


                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,


                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.CommunicationOfficer.Where(m => m.CustomerId == id).Select(model =>
                 new CommunicationOfficerVm
                 {
                     Id = model.Id,
                     Name = model.Name,
                     JobeName = model.JobName,
                     CustomerId = model.CustomerId ?? 0,
                     PhoneNumber = model.PhoneNumber,
                     CreatedDate = model.CreatedDate,
                     Email = model.Email,


                     UpdatedBy = model.UpdatedBy,
                     UpdatedDate = model.UpdatedDate,


                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }


        public bool Save(CommunicationOfficerVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var SaveChange = Context.Entry(Customers.Entities.CommunicationOfficer.Clone(viewModel));
                SaveChange.State = EntityState.Modified;
            }
            else
            {
                Context.CommunicationOfficer.Add(Customers.Entities.CommunicationOfficer.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public CommunicationOfficerVm GetById(int id)
        {
            IQueryable<CommunicationOfficerVm> Details;
            if (ResourcesReader.IsArabic)
            {
                Details = Context.CommunicationOfficer.Where(m => m.Id == id).Select(model =>
                new CommunicationOfficerVm
                {
                    Id = model.Id,
                    Name = model.Name,
                    JobeName = model.JobName,
                    CustomerId = model.CustomerId ?? 0,
                    Email = model.Email,
                    CreatedDate = model.CreatedDate,
                    PhoneNumber = model.PhoneNumber,
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,
                    
                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.CommunicationOfficer.Where(m => m.Id == id).Select(model =>
                new CommunicationOfficerVm
                {
                    Id = model.Id,
                    Name = model.Name,
                    JobeName = model.JobName,
                    CustomerId = model.CustomerId ?? 0,
                    Email = model.Email,
                    CreatedDate = model.CreatedDate,
                    PhoneNumber = model.PhoneNumber,
                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }
    }
}