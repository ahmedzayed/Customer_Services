﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Entities;

namespace Customers.Services.BasicInput
{
 public   class WorkContractService : BaseService, IWorkContractService
    {
        public bool Delete(int id)
        {
            Context.Entry(new Customers.Entities.WorkContract { Id = id }).State = System.Data.Entity.EntityState.Deleted;
            return ContextSaveChanges();
        }

        public List<WorkContractVm> FindAll(WorkContractSm searchModel)
        {
            IQueryable<WorkContractVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = from data in Context.WorkContract
                          select new WorkContractVm
                          {
                              Id = data.Id,
                              StartDate = data.StartDate,
                              EmployeeId = data.EmployeeId ?? 0,
                              EndDate=data.EndDate,


                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            else
            {
                Details = from data in Context.WorkContract
                          select new WorkContractVm
                          {
                              Id = data.Id,
                              StartDate = data.StartDate,
                              EmployeeId = data.EmployeeId ?? 0,
                              EndDate = data.EndDate,


                              UpdatedBy = data.UpdatedBy,
                              UpdatedDate = data.UpdatedDate,

                              CreatedBy = data.CreatedBy ?? 0
                          };
            }
            return Details.ToList();
        }




        public List<WorkContractVm> GetByEmployeeId(int? id)
        {
            IQueryable<WorkContractVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.WorkContract.Where(m => m.EmployeeId == id).Select(model =>
                new WorkContractVm
                {
                    Id = model.Id,
                    StartDate = model.StartDate,
                    EmployeeId = model.EmployeeId ?? 0,
                    EndDate = model.EndDate,


                  

                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.WorkContract.Where(m => m.EmployeeId == id).Select(model =>
                 new WorkContractVm
                 {
                     Id = model.Id,
                     StartDate = model.StartDate,
                     EmployeeId = model.EmployeeId ?? 0,
                     EndDate = model.EndDate,


                     UpdatedBy = model.UpdatedBy,
                     UpdatedDate = model.UpdatedDate,

                     CreatedBy = model.CreatedBy ?? 0
                 });
            }
            return Details.ToList();
        }


        public bool Save(WorkContractVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var SaveChange = Context.Entry(WorkContract.Clone(viewModel));
                SaveChange.State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                Context.WorkContract.Add(WorkContract.Clone(viewModel));
            }
            return ContextSaveChanges();
        }


        public WorkContractVm GetById(int id)
        {
            IQueryable<WorkContractVm> Details;
            if (Helpers.Utilities.ResourcesReader.IsArabic)
            {
                Details = Context.WorkContract.Where(m => m.Id == id).Select(model =>
                new WorkContractVm
                {
                    Id = model.Id,
                    StartDate = model.StartDate,
                    EmployeeId = model.EmployeeId ?? 0,
                    EndDate = model.EndDate,


                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            else
            {
                Details = Context.WorkContract.Where(m => m.Id == id).Select(model =>
                new WorkContractVm
                {
                    Id = model.Id,
                    StartDate = model.StartDate,
                    EmployeeId = model.EmployeeId ?? 0,
                    EndDate = model.EndDate,


                    UpdatedBy = model.UpdatedBy,
                    UpdatedDate = model.UpdatedDate,

                    CreatedBy = model.CreatedBy ?? 0
                });
            }
            return Details.FirstOrDefault();
        }
    }
}