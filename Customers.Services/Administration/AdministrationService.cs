﻿using Customers.Abstracts.Administration;
using Customers.Entities;
using Customers.Model.Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Services.Administration
{
 public   class AdministrationService : BaseService, IAdministrationServiec
    {
        public AuthUserVm CheckUser(string userName, string password)
        {

            var data = Context.Users.Where(a => a.UserName == userName && a.Password == password).Select(model => new AuthUserVm
            {
                UserName = model.UserName,
                Password = model.Password,
                IsActive = model.IsActive,
                ISAdmin = model.IsAdmin,
                TypeId = model.TypeId ?? 0,
                Id = model.Id,
                UserId = model.UserId ??0,
               Name=model.UserName 
                
                

            }).FirstOrDefault();

            return data;
        }
        public bool Save(AuthUserVm viewModel)
        {

            if (viewModel.Id > 0)
            {
                var Servicesd = Context.Entry(Users.Clone(viewModel));
                Servicesd.State = EntityState.Modified;
            }
            else
            {
                Context.Users.Add(Users.Clone(viewModel));
            }
            return ContextSaveChanges();
        }
    }
}
